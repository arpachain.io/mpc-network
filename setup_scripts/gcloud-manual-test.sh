#!/bin/bash

# ssh-keygen
# cat ~/.ssh/id_rsa.pub
## ADD PUBLIC KEY TO GITHUB 
# cd $GOPATH/src/github.com
# mkdir arpachain
# cd arpachain
# git clone git@github.com:arpachain/mpc.git

set -ex
export GOROOT=/usr/local/go
export GOPATH=~/go
export PATH=$PATH:$GOPATH/bin:$GOROOT/bin:~/protoc-3.6.1/bin
export PATH=$PATH:~/.npm-global/bin
export NPM_CONFIG_PREFIX=~/.npm-global
cd $GOPATH/src/gitlab.com/arpachain/mpc
git submodule init
git submodule update --remote
cd $GOPATH/src/gitlab.com/arpachain/mpc/coordinator
dep ensure -v
cd $GOPATH/src/gitlab.com/arpachain/mpc/datagateway
dep ensure -v
cd $GOPATH/src/gitlab.com/arpachain/mpc/eth
dep ensure -v
cd $GOPATH/src/gitlab.com/arpachain/mpc/smart_contract
npm install
cd $GOPATH/src/gitlab.com/arpachain/mpc/worker 
echo "ROOT = $GOPATH/src/gitlab.com/arpachain/mpc/worker" >> CONFIG.mine
echo "OSSL = $HOME/openssl-1.1.1" >> CONFIG.mine
echo 'FLAGS = -DMAX_MOD_SZ=7' >> CONFIG.mine
echo 'OPT = -O3' >> CONFIG.mine
echo 'LDFLAGS =' >> CONFIG.mine     
cp Auto-Test-Data/Cert-Store/* Cert-Store/
cp Auto-Test-Data/1/* Data/
cd $GOPATH/src/gitlab.com/arpachain/mpc
make && make check
