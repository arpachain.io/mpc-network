#!/bin/bash
# sudo apt-get update
# sudo apt-get upgrade
# sudo reboot
set -ex
export GOROOT=/usr/local/go
export GOPATH=~/go
export PATH=$PATH:$GOPATH/bin:$GOROOT/bin:~/protoc-3.6.1/bin
export PATH=$PATH:~/.npm-global/bin
export NPM_CONFIG_PREFIX=~/.npm-global
# install required tools, gcc-7, and g++-7
set -ex \
  && sudo apt-get update \
  && sudo apt-get --yes install psmisc \
  && sudo apt-get --yes install apt-utils \
  && sudo apt-get --yes upgrade \
  && sudo apt-get --yes install unzip \
  && sudo apt-get --yes install wget \
  && sudo apt-get --yes install curl \
  && sudo apt-get --yes install bzip2 \
  && sudo apt-get --yes install git \
  && sudo apt-get --yes install build-essential autoconf libtool pkg-config \
  && sudo apt-get --yes install software-properties-common \
  && sudo add-apt-repository 'deb http://deb.debian.org/debian buster main' \
  && sudo apt-get update \
  && sudo apt-get --yes install -t buster gcc-7 \
  && sudo apt-get --yes install -t buster g++-7 \
  && sudo rm /usr/bin/cpp /usr/bin/gcc /usr/bin/g++ \
  && sudo ln -s /usr/bin/cpp-7 /usr/bin/cpp \
  && sudo ln -s /usr/bin/gcc-7 /usr/bin/gcc \
  && sudo ln -s /usr/bin/g++-7 /usr/bin/g++ \
  && gcc -v
# install gRPC and protobuf
set -ex \
  && sudo apt-get update \
  && sudo apt-get --yes install libgflags-dev libgtest-dev \
  && sudo apt-get --yes install clang libc++-dev \
  && cd \
  && git clone -b $(curl -L https://grpc.io/release) https://github.com/grpc/grpc \
  && cd grpc \
  && git submodule update --init \
  && make && sudo make install \
  && make grpc_cli \
  && sudo ln -s /home/$USER/grpc/bins/opt/grpc_cli /usr/bin/grpc_cli \
  && cd third_party/protobuf \
  && sudo make install
# install utils including glog and gflag 
set -ex \
  && sudo apt-get update \
  && sudo apt-get --yes install cmake \
  && cd \
  && git clone https://github.com/gflags/gflags \
  && cd gflags \
  && mkdir build \
  && cd build \
  && cmake -DCMAKE_INSTALL_PREFIX=/usr/local -DBUILD_SHARED_LIBS=ON -G"Unix Makefiles" .. \
  && make -j4 && sudo make install && sudo ldconfig \
  && cd \
  && git clone https://github.com/google/glog \
  && cd glog \
  && mkdir build \
  && cd build \
  && cmake -D BUILD_gflags_LIBS=ON -D BUILD_SHARED_LIBS=ON -D BUILD_gflags_nothreads_LIBS=ON -D GFLAGS_NAMESPACE=ON .. \
  && make -j4 && sudo make install
# install go, go tools, dep, and protoc-gen-go plugin
set -ex \
  && sudo apt-get update \
  && mkdir -p /usr/local/ \
  && cd \
  && wget https://dl.google.com/go/go1.11.2.linux-amd64.tar.gz \
  && sudo tar -C /usr/local -xzf go1.11.2.linux-amd64.tar.gz \
  && go version \
  && go env \
  && go tool \
  && go get -u -v google.golang.org/grpc \
  && cd \
  && wget https://github.com/protocolbuffers/protobuf/releases/download/v3.6.1/protoc-3.6.1-linux-x86_64.zip \
  && unzip protoc-3.6.1-linux-x86_64.zip -d protoc-3.6.1 \
  && protoc --version \
  && go get -u -v github.com/golang/protobuf/protoc-gen-go \
  && go get -u -v github.com/golang/dep/cmd/dep \
  && dep version \
  && go get -u -v github.com/golang/mock/gomock \
  && go install github.com/golang/mock/mockgen \
  && go get -u -v github.com/processout/grpc-go-pool
# install prerequisite for SCALE-MAMBA(ARPA-Fork) (yasm, mpir, gmpy2, openssl, boost-filesystem)
set -ex \
  && sudo apt-get update \
  && sudo apt-get install libboost-filesystem-dev --yes \
  && sudo apt-get --yes install yasm \
  && cd \
  && wget http://mpir.org/mpir-3.0.0.tar.bz2 \
  && tar -xvf mpir-3.0.0.tar.bz2 \
  && cd mpir-3.0.0 \
  && ./configure --enable-cxx \
  && make && make check && sudo make install \
  && sudo apt-get --yes install python-dev \
  && sudo apt-get --yes install python-gmpy2 \
  && cd \
  && wget https://www.openssl.org/source/openssl-1.1.1.tar.gz \
  && tar -xvf openssl-1.1.1.tar.gz \
  && cd openssl-1.1.1 \
  && ./config && make && sudo make install \
  && cd \
  && echo '/usr/local/lib' | sudo tee -a /etc/ld.so.conf.d/libs.conf \
  && sudo ldconfig
# install nodejs, npm, truffle, ganache-cli, and geth
set -ex \
  && cd \
  && sudo apt-get update \
  && curl -sL https://deb.nodesource.com/setup_11.x | sudo bash - \
  && sudo apt-get --yes install nodejs \
  && node --version \
  && npm --version \
  && mkdir ~/.npm-global \
  && npm config set prefix '~/.npm-global' \
  && npm install --global --unsafe-perm truffle@v5.0.1 \
  && truffle version \
  && npm install --global --unsafe-perm ganache-cli \
  && ganache-cli --version \
  && npm install --global --unsafe-perm openzeppelin-solidity@v2.1.0-rc.2 \
  && go get -d -v  github.com/ethereum/go-ethereum \
  && cd $GOPATH/src/github.com/ethereum/go-ethereum \
  && go install ./... \
  && make \
  && make devtools \
  && geth version \
  && sudo curl -o /usr/bin/solc -fL https://github.com/ethereum/solidity/releases/download/v0.5.4/solc-static-linux \
  && sudo chmod +x /usr/bin/solc \
  && solc --version
