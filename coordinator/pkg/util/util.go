package util

import (
	"github.com/rs/xid"
)

func GenerateID() (string, error) {
	id := xid.New()
	return id.String(), nil
}
