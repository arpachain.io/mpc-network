package util_test

import (
	"testing"

	"gitlab.com/arpachain/mpc/coordinator/pkg/util"
	"github.com/stretchr/testify/assert"
)

var (
	numDigitsHex = 42
)

func TestGenerateID(t *testing.T) {
	assert := assert.New(t)
	idSample := "bg1mqqp9mv6hf94pr630"

	id, err := util.GenerateID()
	assert.NoError(err)
	assert.Len(id, len(idSample))
	t.Logf("ID generated: %v", id)
}
