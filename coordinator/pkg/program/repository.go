package program

import (
	"crypto/sha256"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"path/filepath"
	"sync"
)

// Repository for node provides CRUD operations to persist program bytecode
type Repository interface {
	GetAll() ([]Program, error)
	GetByID(id string) (Program, error)
	Create(program *Program) (string, error)
	Remove(id string)
}

// Reference implementation:

type repository struct {
	mu       *sync.Mutex
	storeMap map[string]Program /*key: ProgramHash*/
}

// NewRepository returns an instance of program.Repository
func NewRepository(programsPath string) Repository {
	repository := &repository{
		mu:       &sync.Mutex{},
		storeMap: map[string]Program{},
	}
	log.Printf("loading mpc programs from: %v", programsPath)
	err := repository.loadDefaultPrograms(programsPath)
	if err != nil {
		log.Panicf("loading mpc programs failed: %v", err)
	}
	return repository
}

func (r *repository) GetAll() ([]Program, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	programs := make([]Program, 0)
	for _, program := range r.storeMap {
		programs = append(programs, program)
	}
	return programs, nil
}

func (r *repository) GetByID(id string) (Program, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	if program, exists := r.storeMap[id]; exists {
		return program, nil
	}
	return Program{}, errors.New("program does not exist")
}

// TODO(bomo): Create currently does not persist the program to the disk
// Disk persistence be implemented in the BadgerDB implementation

// TODO(bomo): ID should eventually be the real hash of the program bytecode
func (r *repository) Create(program *Program) (string, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	if _, exists := r.storeMap[program.ID]; exists {
		return "", errors.New("program with same id already exists")
	}
	r.storeMap[program.ID] = Program{
		ID:         program.ID,
		ProgramMap: program.ProgramMap,
		Schedule:   program.Schedule,
	}
	return program.ID, nil
}

func (r *repository) Remove(id string) {
	r.mu.Lock()
	defer r.mu.Unlock()
	delete(r.storeMap, id)
}

func (r *repository) loadDefaultPrograms(defaultProgramsPath string) error {
	dirs, err := ioutil.ReadDir(defaultProgramsPath)
	if err != nil {
		return err
	}
	for _, dir := range dirs {
		program := &Program{
			ProgramMap: make(map[string][]byte),
			Schedule:   "",
		}
		if dir.IsDir() {
			programName := dir.Name()
			currentPath := defaultProgramsPath + "/" + programName
			log.Printf("looking for programs in: %v", currentPath)
			files, err := ioutil.ReadDir(currentPath)
			if err != nil {
				return err
			}
			programRawData := make([]byte, 0)
			for _, file := range files {
				fn := file.Name()
				if file.Mode().IsRegular() {
					if filepath.Ext(fn) == ".bc" {
						log.Printf("loading bytecode from file: %v", fn)
						content, err := ioutil.ReadFile(currentPath + "/" + fn)
						if err != nil {
							return err
						}
						program.ProgramMap[fn[:len(fn)-3]] = content
						programRawData = append(programRawData, content...)
					}
					if filepath.Ext(fn) == ".sch" {
						log.Printf("loading schedule from file: %v", fn)
						content, err := ioutil.ReadFile(currentPath + "/" + fn)
						if err != nil {
							return err
						}
						program.Schedule = string(content)
						programRawData = append(programRawData, content...)
					}
				}
			}
			if len(programRawData) > 0 {
				programHash := sha256.Sum256(programRawData)
				log.Printf("program hash for %v is: %x", programName, programHash)
				program.ID = fmt.Sprintf("%x", programHash)
				_, err = r.Create(program)
				if err != nil {
					return err
				}
			}
		}
	}
	return nil
}
