package program_test

import (
	"os"
	"testing"

	"gitlab.com/arpachain/mpc/coordinator/pkg/program"
	tu "gitlab.com/arpachain/mpc/coordinator/test"
	"github.com/stretchr/testify/assert"
)

var (
	defaultProgramPath = os.Getenv("GOPATH") +
		"/src/gitlab.com/arpachain/mpc/coordinator/data/program"
)

func TestCreateAndGetByID(t *testing.T) {
	assert := assert.New(t)
	repository := program.NewRepository(defaultProgramPath)
	programToCreate := tu.GenerateTestProgram()

	t.Log("case 1: testing Create...")
	id, err := repository.Create(programToCreate)
	assert.NoError(err)
	assert.NotEmpty(id)

	t.Log("case 2: testing GetByID...")
	createdProgram, err := repository.GetByID(id)
	assert.NoError(err)
	assert.NotEmpty(createdProgram)
	assert.Equal(createdProgram.ID, id)

	t.Log("case 3: testing GetByID with an invalid id...")
	createdProgram, err = repository.GetByID("invalid_id")
	assert.Error(err, "should fail for invalid id")
	assert.Empty(createdProgram, "should return an empty node")
}

func TestGetAll(t *testing.T) {
	assert := assert.New(t)
	repository := program.NewRepository(defaultProgramPath)
	programs, err := repository.GetAll()
	assert.NoError(err, "retrieval should be successful")
	assert.NotEmpty(programs, "there should be default programs retrieved")
	program := programs[0]
	assert.NotEmpty(program.ID)
}
