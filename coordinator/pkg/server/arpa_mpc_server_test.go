package server_test

import (
	"context"
	"errors"
	"fmt"
	"log"
	"os"
	"testing"
	"time"

	"gitlab.com/arpachain/mpc/coordinator/pkg/sharing_data"

	pb "gitlab.com/arpachain/mpc/coordinator/api"
	mock_pb "gitlab.com/arpachain/mpc/coordinator/mock/api"
	"gitlab.com/arpachain/mpc/coordinator/mock/blockchain"
	"gitlab.com/arpachain/mpc/coordinator/mock/certificate"
	"gitlab.com/arpachain/mpc/coordinator/mock/group"
	"gitlab.com/arpachain/mpc/coordinator/mock/mpc"
	"gitlab.com/arpachain/mpc/coordinator/mock/node"
	"gitlab.com/arpachain/mpc/coordinator/mock/program"
	"gitlab.com/arpachain/mpc/coordinator/mock/server"
	"gitlab.com/arpachain/mpc/coordinator/mock/sharing_data"
	"gitlab.com/arpachain/mpc/coordinator/pkg/group"
	"gitlab.com/arpachain/mpc/coordinator/pkg/node"
	"gitlab.com/arpachain/mpc/coordinator/pkg/program"
	"gitlab.com/arpachain/mpc/coordinator/pkg/server"
	srv "gitlab.com/arpachain/mpc/coordinator/pkg/server"
	"github.com/golang/mock/gomock"
	"github.com/processout/grpc-go-pool"
	"github.com/stretchr/testify/assert"
)

var (
	resultChannelMap *map[string]chan srv.MPCResult
	nodeIDs          []string
	controller       *gomock.Controller
	dependencies     *serverDependencies
	serverToTest     pb.ArpaMpcServer
	maxNumWorkers    uint32
)

func TestMain(m *testing.M) {
	retCode := m.Run()
	os.Exit(retCode)
}

type setBehavior func(*serverDependencies)

func setup(t *testing.T, fns ...setBehavior) func(t *testing.T) {
	log.Println("setting up test...")
	maxNumWorkers = 10
	nodeIDs = []string{"fake_id_1", "fake_id_2", "fake_id_3"}
	resChanMap := make(map[string]chan srv.MPCResult)
	resultChannelMap = &resChanMap
	controller := gomock.NewController(t)
	dependencies = createServerDependencies(controller)
	for _, fn := range fns {
		fn(dependencies)
	}
	serverToTest = createServerToTest(dependencies, resultChannelMap)
	return func(t *testing.T) {
		log.Println("tearing down test...")
		maxNumWorkers = 0
		nodeIDs = nil
		resultChannelMap = nil
		controller.Finish()
		controller = nil
		dependencies = nil
		serverToTest = nil
	}
}

type serverDependencies struct {
	programRepository       *mock_program.MockRepository
	sharingDataRepository   *mock_sharing_data.MockRepository
	mapper                  *mock_blockchain.MockMapper
	nodeManager             *mock_node.MockManager
	groupManager            *mock_group.MockManager
	mpcManager              *mock_mpc.MockManager
	mpcWorkerClient         *mock_pb.MockMpcWorkerClient
	blochchainAdapterClient        *mock_pb.MockBlochchainAdapterClient
	workerClientFactory     *mock_server.MockWorkerClientFactory
	blochchainAdapterClientFactory *mock_server.MockBlochchainAdapterClientFactory
	certificateGenerator    *mock_certificate.MockGenerator
}

func createServerDependencies(ctrl *gomock.Controller) *serverDependencies {
	return &serverDependencies{
		programRepository:       mock_program.NewMockRepository(ctrl),
		sharingDataRepository:   mock_sharing_data.NewMockRepository(ctrl),
		mapper:                  mock_blockchain.NewMockMapper(ctrl),
		nodeManager:             mock_node.NewMockManager(ctrl),
		groupManager:            mock_group.NewMockManager(ctrl),
		mpcManager:              mock_mpc.NewMockManager(ctrl),
		mpcWorkerClient:         mock_pb.NewMockMpcWorkerClient(ctrl),
		blochchainAdapterClient:        mock_pb.NewMockBlochchainAdapterClient(ctrl),
		workerClientFactory:     mock_server.NewMockWorkerClientFactory(ctrl),
		blochchainAdapterClientFactory: mock_server.NewMockBlochchainAdapterClientFactory(ctrl),
		certificateGenerator:    mock_certificate.NewMockGenerator(ctrl),
	}
}

func createServerToTest(
	deps *serverDependencies,
	resultChanMap *map[string]chan srv.MPCResult,
) pb.ArpaMpcServer {
	return server.NewArpaMpcServer(
		maxNumWorkers,
		deps.programRepository,
		deps.sharingDataRepository,
		deps.mapper,
		deps.nodeManager,
		deps.groupManager,
		deps.mpcManager,
		deps.workerClientFactory,
		deps.certificateGenerator,
		resultChanMap,
		deps.blochchainAdapterClientFactory,
	)
}

func createFakeRunMpcRequest(numNodes int) *pb.RunMpcRequest {
	mpcDataList := make([]*pb.MpcData, 0)
	for i := 0; i < numNodes; i++ {
		fakeMpcData := &pb.MpcData{}
		mpcDataList = append(mpcDataList, fakeMpcData)
	}
	req := &pb.RunMpcRequest{
		NumNodes: uint32(numNodes),
		MpcData:  mpcDataList,
	}
	return req
}

func createFakeRunMpcForBlockchainRequest(
	numTotalWorkers int,
	delegatedWorkerBlockchainAddress string,
	dataProviderWorkerBlockchainAddresses []string,
) *pb.RunMpcForBlockchainRequest {
	req := &pb.RunMpcForBlockchainRequest{
		ProgramHash:                    "fake-program-0",
		NumTotalWorkers:                uint32(numTotalWorkers),
		DelegatedWorkerBlockchainAddress:      delegatedWorkerBlockchainAddress,
		DataProviderWorkerBlockchainAddresses: dataProviderWorkerBlockchainAddresses,
	}
	return req
}

func setMockProgramRepositoryBehavior_Valid(m *mock_program.MockRepository) {
	fakeProgramMap := make(map[string][]byte)
	fakeProgramMap["fake-program-0"] = []byte("fake_program_byte_code")
	m.
		EXPECT().
		GetByID(gomock.Any()).
		Return(
			program.Program{
				ID:         "fake_program_id",
				ProgramMap: fakeProgramMap,
				Schedule:   "fake_program_schedule",
			},
			nil,
		).
		AnyTimes()
}

func setMockSharingDataRepositoryBehavior_Valid(m *mock_sharing_data.MockRepository) {
	m.
		EXPECT().
		GetByNumMPC(gomock.Any()).
		Return(sharing_data.SharingData{
			NumMPC: 3,
			Data:   "fake_sharing_data",
		}, nil).
		AnyTimes()
}

func setMockGroupManagerBehavior_Valid(m *mock_group.MockManager, nodeIDs []string) {
	m.
		EXPECT().
		GetAvailableGroup(gomock.Any()).
		Return(&group.Group{
			ID:      "fake_group_id_1",
			NodeIDs: nodeIDs,
		}, nil).
		AnyTimes()
}

func setMockGroupManagerBehavior_Eth_Valid(
	m *mock_group.MockManager,
	dataNodeIDs []string,
	delegatedNodeID string,
	additionalNodeIDs []string,
) {
	allNodeIDs := append(dataNodeIDs, delegatedNodeID)
	allNodeIDs = append(allNodeIDs, additionalNodeIDs...)
	m.
		EXPECT().
		GetAvailableGroupForBlockchain(
			dataNodeIDs,
			delegatedNodeID,
			len(additionalNodeIDs),
		).Return(
		&group.Group{
			ID:      "fake_group_id_1",
			IsBusy:  true,
			NodeIDs: allNodeIDs,
		},
		additionalNodeIDs,
		nil,
	)
}

func setMockGroupManagerBehavior_Ungroup(m *mock_group.MockManager) {
	m.
		EXPECT().
		Ungroup(gomock.Any()).
		Return(nil).
		AnyTimes()
}

func setMockMPCManagerBehavior_Valid(m *mock_mpc.MockManager, nodeIDs []string) {
	m.
		EXPECT().
		Create(nodeIDs).
		Return("fake_mpc_id_1", nil).
		AnyTimes()
}

func setMockNodeManagerBehavior_Valid(m *mock_node.MockManager, nodeIDs []string) {
	nodes := make([]*node.Node, 0)
	for _, nodeID := range nodeIDs {
		nodes = append(
			nodes, &node.Node{
				ID: nodeID,
			},
		)
	}
	m.
		EXPECT().
		GetByIDs(nodeIDs).
		Return(nodes, nil).
		AnyTimes()
}

func setMockNodeManagerBehavior_Valid_Eth(m *mock_node.MockManager, nodeIDs, blockchainAddresses []string) {
	nodes := make([]*node.Node, 0)
	for i, nodeID := range nodeIDs {
		nodes = append(
			nodes, &node.Node{
				ID:         nodeID,
				BlockchainAddress: blockchainAddresses[i],
			},
		)
	}
	m.
		EXPECT().
		GetByIDs(nodeIDs).
		Return(nodes, nil).
		AnyTimes()
}

func setMockNodeManagerBehavior_Valid_Eth_CalledOnce(m *mock_node.MockManager, nodeIDs, blockchainAddresses []string) {
	nodes := make([]*node.Node, 0)
	for i, nodeID := range nodeIDs {
		nodes = append(
			nodes, &node.Node{
				ID:         nodeID,
				BlockchainAddress: blockchainAddresses[i],
			},
		)
	}
	m.
		EXPECT().
		GetByIDs(nodeIDs).
		Return(nodes, nil).
		Times(1)
}

func setMockCertificateGeneratorBehavior_Valid(g *mock_certificate.MockGenerator) {
	g.
		EXPECT().
		GetCACertificate(gomock.Any()).
		Return([]byte("fake_ca_cert"), nil).
		AnyTimes()
}

func setMockMPCWorkerClientFactoryBehavior_Valid(f *mock_server.MockWorkerClientFactory, c *mock_pb.MockMpcWorkerClient) {
	f.
		EXPECT().
		GetWorkerClient(gomock.Any(), gomock.Any()).
		Return(c, &grpcpool.ClientConn{}, nil).
		AnyTimes()
}

func setMockBlochchainAdapterClientFactoryBehavior_Valid(f *mock_server.MockBlochchainAdapterClientFactory, c *mock_pb.MockBlochchainAdapterClient) {
	f.
		EXPECT().
		GetBlochchainAdapterClient(gomock.Any()).
		Return(c, &grpcpool.ClientConn{}, nil).
		AnyTimes()
}

func setMockMPCWorkerClientBehavior_Valid(c *mock_pb.MockMpcWorkerClient) {
	c.
		EXPECT().
		StartMpc(gomock.Any(), gomock.Any()).
		Return(&pb.StartMpcResponse{Success: true}, nil).
		AnyTimes()
}

func setMockBlochchainAdapterClientBehavior_Valid(c *mock_pb.MockBlochchainAdapterClient) {
	c.
		EXPECT().
		ReportMpcResult(gomock.Any(), gomock.Any()).
		Return(&pb.ReportMpcResultResponse{Success: true}, nil).
		AnyTimes()
}

func setMockMapperBehavior_Valid(
	m *mock_blockchain.MockMapper,
	ethMap map[string]string, /*key: BlockchainAddress, value: NodeID*/
) {
	for blockchainAddress, nodeID := range ethMap {
		m.EXPECT().LookUp(blockchainAddress).Return(nodeID, nil).AnyTimes()
	}
}

func TestRunMpc_InvalidRequestedNumNodes(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t)
	defer tearDown(t)

	req := &pb.RunMpcRequest{
		NumNodes: 0,
	}
	res, err := serverToTest.RunMpc(context.Background(), req)
	assert.Empty(res)
	assert.Error(err)
}

func TestRunMpc_InvalidRequestedMpcData(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t)
	defer tearDown(t)

	req := &pb.RunMpcRequest{
		// case where NumNodes != len(MpcData)
		NumNodes: 3,
		MpcData:  []*pb.MpcData{&pb.MpcData{}, &pb.MpcData{}},
	}
	res, err := serverToTest.RunMpc(context.Background(), req)
	assert.Empty(res)
	assert.Error(err)
}

func TestRunMpc_CannotGetAvailableGroup(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, func(deps *serverDependencies) {
		setMockMPCWorkerClientBehavior_Valid(deps.mpcWorkerClient)
		setMockMPCWorkerClientFactoryBehavior_Valid(deps.workerClientFactory, deps.mpcWorkerClient)
		setMockProgramRepositoryBehavior_Valid(deps.programRepository)
		deps.groupManager.
			EXPECT().
			GetAvailableGroup(gomock.Any()).
			Return(nil, errors.New("cannot get available group")).
			AnyTimes()
	})
	defer tearDown(t)

	req := createFakeRunMpcRequest(3)
	res, err := serverToTest.RunMpc(context.Background(), req)
	assert.Empty(res)
	assert.Error(err)
}

func TestRunMpc_CannotCreateMPC(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, func(deps *serverDependencies) {
		setMockGroupManagerBehavior_Valid(deps.groupManager, nodeIDs)
		setMockProgramRepositoryBehavior_Valid(deps.programRepository)
		deps.mpcManager.
			EXPECT().
			Create(nodeIDs).
			Return("", errors.New("cannot create mpc")).
			AnyTimes()
	})
	defer tearDown(t)

	req := createFakeRunMpcRequest(3)
	res, err := serverToTest.RunMpc(context.Background(), req)
	assert.Empty(res)
	assert.Error(err)
}

func TestRunMpc_CannotGetNodes(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, func(deps *serverDependencies) {
		setMockGroupManagerBehavior_Valid(deps.groupManager, nodeIDs)
		setMockMPCManagerBehavior_Valid(deps.mpcManager, nodeIDs)
		setMockProgramRepositoryBehavior_Valid(deps.programRepository)
		deps.nodeManager.
			EXPECT().
			GetByIDs(nodeIDs).
			Return(nil, errors.New("cannot get nodes")).
			AnyTimes()
	})
	defer tearDown(t)

	req := createFakeRunMpcRequest(3)
	res, err := serverToTest.RunMpc(context.Background(), req)
	assert.Empty(res)
	assert.Error(err)
}

func TestRunMpc_CannotGetCA(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, func(deps *serverDependencies) {
		setMockGroupManagerBehavior_Valid(deps.groupManager, nodeIDs)
		setMockMPCManagerBehavior_Valid(deps.mpcManager, nodeIDs)
		setMockNodeManagerBehavior_Valid(deps.nodeManager, nodeIDs)
		setMockProgramRepositoryBehavior_Valid(deps.programRepository)
		deps.certificateGenerator.
			EXPECT().
			GetCACertificate(gomock.Any()).
			Return(nil, errors.New("cannot get ca cert")).
			AnyTimes()
	})
	defer tearDown(t)

	req := createFakeRunMpcRequest(3)
	res, err := serverToTest.RunMpc(context.Background(), req)
	assert.Empty(res)
	assert.Error(err)
}

func TestRunMpc_CannotGetWorkerClient(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, func(deps *serverDependencies) {
		setMockGroupManagerBehavior_Valid(deps.groupManager, nodeIDs)
		setMockMPCManagerBehavior_Valid(deps.mpcManager, nodeIDs)
		setMockNodeManagerBehavior_Valid(deps.nodeManager, nodeIDs)
		setMockCertificateGeneratorBehavior_Valid(deps.certificateGenerator)
		setMockSharingDataRepositoryBehavior_Valid(deps.sharingDataRepository)
		setMockProgramRepositoryBehavior_Valid(deps.programRepository)
		deps.workerClientFactory.
			EXPECT().
			GetWorkerClient(gomock.Any(), gomock.Any()).
			Return(nil, nil, errors.New("cannot get worker client")).
			AnyTimes()
	})
	defer tearDown(t)

	req := createFakeRunMpcRequest(3)
	res, err := serverToTest.RunMpc(context.Background(), req)
	assert.Empty(res)
	assert.Error(err)
}

func TestRunMpc_CannotStartMPC(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, func(deps *serverDependencies) {
		setMockGroupManagerBehavior_Valid(deps.groupManager, nodeIDs)
		setMockMPCManagerBehavior_Valid(deps.mpcManager, nodeIDs)
		setMockNodeManagerBehavior_Valid(deps.nodeManager, nodeIDs)
		setMockCertificateGeneratorBehavior_Valid(deps.certificateGenerator)
		setMockSharingDataRepositoryBehavior_Valid(deps.sharingDataRepository)
		setMockProgramRepositoryBehavior_Valid(deps.programRepository)
		deps.mpcWorkerClient.
			EXPECT().
			StartMpc(gomock.Any(), gomock.Any()).
			Return(nil, errors.New("cannot start mpc")).
			AnyTimes()
		setMockMPCWorkerClientFactoryBehavior_Valid(deps.workerClientFactory, deps.mpcWorkerClient)
	})
	defer tearDown(t)

	req := createFakeRunMpcRequest(3)
	res, err := serverToTest.RunMpc(context.Background(), req)
	assert.Empty(res)
	assert.Error(err)
}

func TestRunMpc_MPCResultHasError(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, func(deps *serverDependencies) {
		setMockGroupManagerBehavior_Valid(deps.groupManager, nodeIDs)
		setMockMPCManagerBehavior_Valid(deps.mpcManager, nodeIDs)
		setMockNodeManagerBehavior_Valid(deps.nodeManager, nodeIDs)
		setMockCertificateGeneratorBehavior_Valid(deps.certificateGenerator)
		setMockSharingDataRepositoryBehavior_Valid(deps.sharingDataRepository)
		setMockMPCWorkerClientBehavior_Valid(deps.mpcWorkerClient)
		setMockMPCWorkerClientFactoryBehavior_Valid(deps.workerClientFactory, deps.mpcWorkerClient)
		setMockProgramRepositoryBehavior_Valid(deps.programRepository)
	})
	defer tearDown(t)

	req := createFakeRunMpcRequest(3)
	fakeMPCResult := srv.MPCResult{
		Err:   errors.New("fake mpc result error"),
		MpcID: "fake_mpc_id_1",
	}
	go func() {
		time.Sleep(time.Millisecond * 500)
		(*resultChannelMap)["fake_mpc_id_1"] <- fakeMPCResult
	}()
	res, err := serverToTest.RunMpc(context.Background(), req)
	assert.Empty(res)
	assert.Error(err)
}

func TestRunMpc(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, func(deps *serverDependencies) {
		setMockGroupManagerBehavior_Valid(deps.groupManager, nodeIDs)
		setMockGroupManagerBehavior_Ungroup(deps.groupManager)
		setMockMPCManagerBehavior_Valid(deps.mpcManager, nodeIDs)
		setMockNodeManagerBehavior_Valid(deps.nodeManager, nodeIDs)
		setMockCertificateGeneratorBehavior_Valid(deps.certificateGenerator)
		setMockSharingDataRepositoryBehavior_Valid(deps.sharingDataRepository)
		setMockMPCWorkerClientBehavior_Valid(deps.mpcWorkerClient)
		setMockMPCWorkerClientFactoryBehavior_Valid(deps.workerClientFactory, deps.mpcWorkerClient)
		setMockProgramRepositoryBehavior_Valid(deps.programRepository)
	})
	defer tearDown(t)

	req := createFakeRunMpcRequest(3)
	for i := 0; i < 3; i++ {
		fakeMPCResult := srv.MPCResult{
			Err:   nil,
			MpcID: "fake_mpc_id_1",
			MpcResult: &pb.MpcResult{
				Data: "fake_mpc_result_data",
			},
		}
		go func() {
			time.Sleep(time.Millisecond * 500)
			(*resultChannelMap)["fake_mpc_id_1"] <- fakeMPCResult
		}()
	}
	res, err := serverToTest.RunMpc(context.Background(), req)
	assert.NotEmpty(res)
	assert.NoError(err)
	assert.Equal(3, len(res.GetMpcFinalResult()))
}

func TestRunMpcForBlockchain(t *testing.T) {
	ethMap := make(map[string]string)

	delegatedWorkerBlockchainAddress := "delegated_worker_blockchain_address_1"
	delegatedNodeID := "delegated_node_id_1"
	ethMap[delegatedWorkerBlockchainAddress] = delegatedNodeID

	dataProviderWorkerBlockchainAddresses := []string{"data_provider_worker_blockchain_address_1", "data_provider_worker_blockchain_address_2"}
	dataNodeIDs := []string{"data_node_id_1", "data_node_id_2"}
	for i := 0; i < len(dataNodeIDs); i++ {
		ethMap[dataProviderWorkerBlockchainAddresses[i]] = dataNodeIDs[i]
	}

	additionalNodeIDs := []string{"additional_node_id_1", "additional_node_id_2", "additional_node_id_3"}
	additionalWorkerBlockchainAddresses := []string{"additional_worker_blockchain_address_1", "additional_worker_blockchain_address_2", "additional_worker_blockchain_address_3"}

	numTotalNodeIDs := 1 + len(dataNodeIDs) + len(additionalNodeIDs)
	allNodeIDs := append(dataNodeIDs, delegatedNodeID)
	allNodeIDs = append(allNodeIDs, additionalNodeIDs...)
	allWorkerBlockchainAddresses := append(dataProviderWorkerBlockchainAddresses, delegatedWorkerBlockchainAddress)
	allWorkerBlockchainAddresses = append(allWorkerBlockchainAddresses, additionalWorkerBlockchainAddresses...)
	mpcID := "fake_mpc_id_1"
	assert := assert.New(t)
	tearDown := setup(t, func(deps *serverDependencies) {
		setMockProgramRepositoryBehavior_Valid(deps.programRepository)
		setMockSharingDataRepositoryBehavior_Valid(deps.sharingDataRepository)
		setMockMapperBehavior_Valid(deps.mapper, ethMap)
		setMockGroupManagerBehavior_Eth_Valid(deps.groupManager, dataNodeIDs, delegatedNodeID, additionalNodeIDs)
		setMockGroupManagerBehavior_Valid(deps.groupManager, allNodeIDs)
		setMockGroupManagerBehavior_Ungroup(deps.groupManager)
		setMockMPCManagerBehavior_Valid(deps.mpcManager, allNodeIDs)
		setMockNodeManagerBehavior_Valid_Eth_CalledOnce(deps.nodeManager, additionalNodeIDs, additionalWorkerBlockchainAddresses)
		setMockNodeManagerBehavior_Valid_Eth(deps.nodeManager, allNodeIDs, allWorkerBlockchainAddresses)
		setMockCertificateGeneratorBehavior_Valid(deps.certificateGenerator)
		setMockMPCWorkerClientBehavior_Valid(deps.mpcWorkerClient)
		setMockBlochchainAdapterClientBehavior_Valid(deps.blochchainAdapterClient)
		setMockMPCWorkerClientFactoryBehavior_Valid(deps.workerClientFactory, deps.mpcWorkerClient)
		setMockBlochchainAdapterClientFactoryBehavior_Valid(deps.blochchainAdapterClientFactory, deps.blochchainAdapterClient)
	})
	defer tearDown(t)

	req := createFakeRunMpcForBlockchainRequest(numTotalNodeIDs, delegatedWorkerBlockchainAddress, dataProviderWorkerBlockchainAddresses)
	for i := 0; i < numTotalNodeIDs; i++ {
		fakeMPCResult := srv.MPCResult{
			Err:   nil,
			MpcID: mpcID,
			MpcResult: &pb.MpcResult{
				Data: fmt.Sprintf("fake_mpc_result_data_%d", i),
			},
		}
		go func() {
			time.Sleep(time.Millisecond * 500)
			(*resultChannelMap)[mpcID] <- fakeMPCResult
		}()
	}
	res, err := serverToTest.RunMpcForBlockchain(context.Background(), req)
	assert.NotEmpty(res)
	assert.NoError(err)
	assert.Equal(additionalWorkerBlockchainAddresses, res.GetAdditionalWorkerBlockchainAddresses())
	assert.Equal(mpcID, res.GetMpcId())
	// Wait to make sure ReportMpcResult gets called
	time.Sleep(time.Millisecond * 1000)
}
