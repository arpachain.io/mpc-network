package group

import (
	"errors"
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/arpachain/mpc/coordinator/pkg/node"
	"gitlab.com/arpachain/mpc/coordinator/pkg/worker_status_updater"
)

// Manager for group provides necessary functions for mpc server to interact with groups
type Manager interface {
	CreateGroup(size int) (string, error)
	Ungroup(groupIDs []string) error
	AddToGroup(groupID string, nodeIDs []string) error
	GetNodesFromGroup(groupID string) ([]*node.Node, error)
	GetAvailableGroup(size int) (*Group, error)
	GetAvailableGroupForBlockchain(
		dataNodeIDs []string,
		delegatedNodeID string,
		numAdditionalNodes int,
	) (*Group, []string /*Additional NodeIDs*/, error)
}

// Reference implementation:

type manager struct {
	nodeRepository  node.Repository
	groupRepository Repository
	rand            *rand.Rand
	updater         worker_status_updater.Updater
}

// NewManager returns an instance of group.Manager
func NewManager(
	nodeRepository node.Repository,
	groupRepository Repository,
	updater worker_status_updater.Updater,
) Manager {
	return &manager{
		nodeRepository:  nodeRepository,
		groupRepository: groupRepository,
		rand:            rand.New(rand.NewSource(time.Now().Unix())),
		updater:         updater,
	}
}

func (m *manager) CreateGroup(size int) (string /*GroupID*/, error) {
	pickedNodeIDs, err := m.pickAvailableNodes(size, make(map[string]bool)) /*empty map means nothing is excluded*/
	if err != nil {
		return "", err
	}
	return m.createGroup(pickedNodeIDs)
}

func (m *manager) Ungroup(groupIDs []string) error {
	if len(groupIDs) < 1 {
		return errors.New("no group id")
	}
	for _, groupID := range groupIDs {
		group, err := m.groupRepository.GetByID(groupID)
		if err != nil {
			return err
		}
		nodeIDs := group.NodeIDs
		for _, nodeID := range nodeIDs {
			node, err := m.nodeRepository.GetByID(nodeID)
			if err != nil {
				return err
			}
			m.nodeRepository.UngroupNode(node.ID)
		}
		m.groupRepository.Remove(groupID)
	}
	m.updater.SendUpdate()
	return nil
}

func (m *manager) AddToGroup(groupID string, nodeIDs []string) error {
	group, err := m.groupRepository.GetByID(groupID)
	if err != nil {
		return err
	}
	if group.IsBusy {
		return errors.New("cannot add nodes to a busy group")
	}
	for _, nodeID := range nodeIDs {
		node, err := m.nodeRepository.GetByID(nodeID)
		if err != nil {
			return err
		}
		if node.IsGrouped {
			return fmt.Errorf("cannot add already grouped node: %v", node.ID)
		}
		err = m.nodeRepository.GroupNode(nodeID)
		if err != nil {
			return err
		}
	}
	oldNodeIDs := group.NodeIDs
	newNodeIDs := append(oldNodeIDs, nodeIDs...)
	return m.groupRepository.UpdateNodeIDs(group.ID, newNodeIDs)
}

func (m *manager) GetNodesFromGroup(groupID string) ([]*node.Node, error) {
	group, err := m.groupRepository.GetByID(groupID)
	if err != nil {
		return nil, err
	}
	nodeIDs := group.NodeIDs
	nodes := make([]*node.Node, len(nodeIDs))
	for i, nodeID := range nodeIDs {
		node, err := m.nodeRepository.GetByID(nodeID)
		if err != nil {
			return nil, err
		}
		nodes[i] = &node
	}
	return nodes, err
}

// TODO(bomo): free groups should be released
func (m *manager) GetAvailableGroup(size int) (*Group, error) {
	group, err := m.groupRepository.GetAvailableGroup(size)
	if err != nil {
		newGroupID, err := m.CreateGroup(size)
		if err != nil {
			return nil, err
		}
		group, err = m.groupRepository.GetByID(newGroupID)
	}
	err = m.groupRepository.UpdateIsBusy(group.ID, true /*mark the group as busy*/)
	group.IsBusy = true
	if err != nil {
		return nil, err
	}
	m.updater.SendUpdate()
	return &group, nil
}

func (m *manager) GetAvailableGroupForBlockchain(
	dataNodeIDs []string,
	delegatedNodeID string,
	numAdditionalNodes int,
) (*Group, []string /*Additional NodeIDs*/, error) {
	requestedNodeIDs := make([]string, 0)
	excludedNodeIDMap := make(map[string]bool)
	// The below guard clauses guarantees that
	// delegated node and data nodes exist and are not grouped
	if len(delegatedNodeID) > 0 {
		delegatedNode, err := m.nodeRepository.GetByID(delegatedNodeID)
		if err != nil {
			return nil, nil, err
		}
		if delegatedNode.IsGrouped {
			return nil, nil, fmt.Errorf("cannot get available group, delegated node: %v is in another group", delegatedNodeID)
		}
		requestedNodeIDs = append(requestedNodeIDs, delegatedNodeID)
		excludedNodeIDMap[delegatedNodeID] = true
	}
	for _, dataNodeID := range dataNodeIDs {
		dataNode, err := m.nodeRepository.GetByID(dataNodeID)
		if err != nil {
			return nil, nil, err
		}
		if dataNode.IsGrouped {
			return nil, nil, fmt.Errorf("cannot get available group, data node: %v is in another group", dataNodeID)
		}
		requestedNodeIDs = append(requestedNodeIDs, dataNodeID)
		excludedNodeIDMap[dataNodeID] = true
	}

	additionalNodeIDs, err := m.pickAvailableNodes(numAdditionalNodes, excludedNodeIDMap)
	if err != nil {
		return nil, nil, err
	}
	finalNodeIDs := append(requestedNodeIDs, additionalNodeIDs...)
	id, err := m.createGroup(finalNodeIDs)
	if err != nil {
		return nil, nil, err
	}
	group, err := m.groupRepository.GetByID(id)
	if err != nil {
		return nil, nil, err
	}
	err = m.groupRepository.UpdateIsBusy(group.ID, true /*mark the group as busy*/)
	group.IsBusy = true
	if err != nil {
		return nil, nil, err
	}
	m.updater.SendUpdate()
	return &group, additionalNodeIDs, nil
}

func (m *manager) pickAvailableNodes(numNodes int, excludedNodeIDMap map[string]bool) ([]string /*NodeIDs*/, error) {
	allNodes, err := m.nodeRepository.GetAll()
	if err != nil {
		return nil, err
	}
	if len(allNodes) < numNodes {
		return nil, errors.New("not enough nodes")
	}
	availableNodeIDs := make([]string, 0)
	for _, node := range allNodes {
		_, isExcluded := excludedNodeIDMap[node.ID]
		if !node.IsGrouped && !isExcluded {
			availableNodeIDs = append(availableNodeIDs, node.ID)
		}
	}
	if len(availableNodeIDs) < numNodes {
		return nil, errors.New("not enough available nodes")
	}
	// randomly pick n (n = size) nodes using random permutation
	pickedNodeIDs := make([]string, 0)
	for _, randIndex := range m.rand.Perm(len(availableNodeIDs))[:numNodes] {
		pickedNodeID := availableNodeIDs[randIndex]
		pickedNodeIDs = append(pickedNodeIDs, pickedNodeID)
	}
	return pickedNodeIDs, nil
}

func (m *manager) createGroup(nodeIDs []string) (string /*groupID*/, error) {
	newGroup := &Group{
		NodeIDs: nodeIDs,
	}
	id, err := m.groupRepository.Create(newGroup)
	if err != nil {
		return "", err
	}
	for _, nodeID := range nodeIDs {
		err := m.nodeRepository.GroupNode(nodeID)
		if err != nil {
			m.groupRepository.Remove(id)
			return "", err
		}
	}
	return id, nil
}
