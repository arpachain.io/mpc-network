package group

// Group as a single group of mpc nodes
type Group struct {
	ID      string
	NodeIDs []string
	IsBusy  bool
}

// GetSize will return the number of nodes in this group
func (g *Group) GetSize() int {
	return len(g.NodeIDs)
}
