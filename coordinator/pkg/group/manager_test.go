package group_test

import (
	"testing"

	"gitlab.com/arpachain/mpc/coordinator/pkg/group"
	"gitlab.com/arpachain/mpc/coordinator/pkg/node"
	"gitlab.com/arpachain/mpc/coordinator/pkg/worker_status_updater"
	tu "gitlab.com/arpachain/mpc/coordinator/test"
	"github.com/stretchr/testify/assert"
)

func TestCreateGroup(t *testing.T) {
	assert := assert.New(t)
	nodeRepository := node.NewRepository()
	groupRepository := group.NewRepository()
	workerStatueUpdater := worker_status_updater.NewUpdater(nodeRepository)
	manager := group.NewManager(nodeRepository, groupRepository, workerStatueUpdater)

	t.Log("case 1: testing CreateGroup when there is no node...")
	id, err := manager.CreateGroup(5)
	assert.Error(err)
	assert.Empty(id)

	t.Log("case 2: testing CreateGroup when there is not enough node...")
	tu.CreateNodes(numNodes, nodeRepository)
	id, err = manager.CreateGroup(numNodes + 1)
	assert.Error(err)
	assert.Empty(id)

	t.Log("case 3: testing CreateGroup when there is not enough available nodes...")
	nodeIDs, _ := nodeRepository.GetAll()
	// mark one node as grouped
	// so that number_of_available < size_required
	nodeRepository.GroupNode(nodeIDs[0].ID)
	id, err = manager.CreateGroup(numNodes)
	assert.Error(err)
	assert.Empty(id)

	t.Log("case 4: testing CreateGroup when there is just enough available nodes...")
	// mark that one node as ungrouped
	// so that number_of_available = size_required
	nodeRepository.UngroupNode(nodeIDs[0].ID)
	id, err = manager.CreateGroup(numNodes)
	assert.NoError(err)
	assert.NotEmpty(id)
	group, err := groupRepository.GetByID(id)
	assert.NoError(err)
	assert.NotEmpty(group)
	assert.Equal(group.ID, id, "id should be properly assgined")
	assert.Equal(group.GetSize(), numNodes, "all nodes should be grouped")
	assert.False(group.IsBusy, "newly created group should be free")
	for _, nodeID := range group.NodeIDs {
		node, _ := nodeRepository.GetByID(nodeID)
		assert.True(node.IsGrouped, "node should be marked as grouped")
	}

	t.Log("case 5: testing CreateGroup when there is more than enough available nodes...")
	// ungroup all nodes
	for _, nodeID := range group.NodeIDs {
		nodeRepository.UngroupNode(nodeID)
	}
	id, err = manager.CreateGroup(numNodes - 1)
	assert.NoError(err)
	assert.NotEmpty(id)
	group, err = groupRepository.GetByID(id)
	assert.NoError(err)
	assert.NotEmpty(group)
	assert.Equal(group.ID, id, "id should be properly assigned")
	assert.Equal(group.GetSize(), numNodes-1, "only required number of nodes should be grouped")
	assert.False(group.IsBusy, "newly created group should be free")
	for _, nodeID := range group.NodeIDs {
		node, _ := nodeRepository.GetByID(nodeID)
		assert.True(node.IsGrouped, "node should be marked as grouped")
	}
}

func TestUngroup(t *testing.T) {
	assert := assert.New(t)
	nodeRepository := node.NewRepository()
	groupRepository := group.NewRepository()
	workerStatueUpdater := worker_status_updater.NewUpdater(nodeRepository)
	manager := group.NewManager(nodeRepository, groupRepository, workerStatueUpdater)

	// create enough nodes for testing first
	nodeIDs := tu.CreateNodes(numNodes, nodeRepository)
	// mark all of them as grouped
	for _, nodeID := range nodeIDs {
		nodeRepository.GroupNode(nodeID)
	}
	// create a new group with the nodes, assuming group.Repository.Create works
	groupToCreate := &group.Group{
		NodeIDs: nodeIDs,
	}
	id, _ := groupRepository.Create(groupToCreate)

	t.Log("case 1: testing Ungroup with empty group id array...")
	err := manager.Ungroup([]string{})
	assert.Error(err)

	t.Log("case 2: testing Ungroup...")
	err = manager.Ungroup([]string{id})
	group, err := groupRepository.GetByID(id)
	assert.Error(err)
	assert.Empty(group)
	for _, nodeID := range nodeIDs {
		node, _ := nodeRepository.GetByID(nodeID)
		assert.False(node.IsGrouped, "each node should be ungrouped now")
	}

}

func TestAddToGroup(t *testing.T) {
	assert := assert.New(t)
	nodeRepository := node.NewRepository()
	groupRepository := group.NewRepository()
	workerStatueUpdater := worker_status_updater.NewUpdater(nodeRepository)
	manager := group.NewManager(nodeRepository, groupRepository, workerStatueUpdater)

	// create enough nodes for testing first
	nodeIDs := tu.CreateNodes(numNodes, nodeRepository)
	// mark all of them as grouped
	for _, nodeID := range nodeIDs {
		nodeRepository.GroupNode(nodeID)
	}
	// create a new group with the nodes, assuming group.Repository.Create works
	groupToCreate := &group.Group{
		NodeIDs: nodeIDs,
	}
	id, _ := groupRepository.Create(groupToCreate)

	t.Log("case 1: testing AddToGroup for invalid group id...")
	err := manager.AddToGroup("fake_id", nodeIDs)
	assert.Error(err)

	t.Log("case 2: testing AddToGroup for busy group...")
	groupRepository.UpdateIsBusy(id, true)
	err = manager.AddToGroup(id, nodeIDs)
	assert.Error(err)
	groupRepository.UpdateIsBusy(id, false)

	t.Log("case 3: testing AddToGroup when some node is already grouped...")
	err = manager.AddToGroup(id, nodeIDs)
	assert.Error(err)

	t.Log("case 4: testing AddToGroup...")
	nodeIDsToAdd := tu.CreateNodes(numNodes, nodeRepository)
	err = manager.AddToGroup(id, nodeIDsToAdd)
	assert.NoError(err)
	group, _ := groupRepository.GetByID(id)
	assert.Equal(group.GetSize(), numNodes*2, "size should be increased")
	for _, nodeID := range group.NodeIDs {
		node, _ := nodeRepository.GetByID(nodeID)
		assert.True(node.IsGrouped, "each node should be grouped")
	}
}

func TestGetNodesFromGroup(t *testing.T) {
	assert := assert.New(t)
	nodeRepository := node.NewRepository()
	groupRepository := group.NewRepository()
	workerStatueUpdater := worker_status_updater.NewUpdater(nodeRepository)
	manager := group.NewManager(nodeRepository, groupRepository, workerStatueUpdater)

	// create enough nodes for testing first
	nodeIDs := tu.CreateNodes(numNodes, nodeRepository)
	// mark all of them as grouped
	for _, nodeID := range nodeIDs {
		nodeRepository.GroupNode(nodeID)
	}
	// create a new group with the nodes, assuming group.Repository.Create works
	groupToCreate := &group.Group{
		NodeIDs: nodeIDs,
	}
	id, _ := groupRepository.Create(groupToCreate)

	t.Log("case 1: testing GetNodesFromGroup with an invalid id...")
	nodes, err := manager.GetNodesFromGroup("fake_id")
	assert.Error(err)
	assert.Empty(nodes)

	t.Log("case 2: testing GetNodesFromGroup...")
	nodes, err = manager.GetNodesFromGroup(id)
	assert.NoError(err)
	assert.NotEmpty(nodes)
	assert.Equal(len(nodes), numNodes, "same number of nodes should be returned")
}

func TestGetAvailableGroupInManager(t *testing.T) {
	assert := assert.New(t)
	nodeRepository := node.NewRepository()
	groupRepository := group.NewRepository()
	workerStatueUpdater := worker_status_updater.NewUpdater(nodeRepository)
	manager := group.NewManager(nodeRepository, groupRepository, workerStatueUpdater)

	// create enough nodes for testing first
	nodeIDs := tu.CreateNodes(numNodes, nodeRepository)
	// mark all of them as grouped
	for _, nodeID := range nodeIDs {
		nodeRepository.GroupNode(nodeID)
	}
	// create a new group with the nodes, assuming group.Repository.Create works
	groupToCreate := &group.Group{
		NodeIDs: nodeIDs,
	}

	groupRepository.Create(groupToCreate)

	t.Log("case 1: testing GetAvailableGroup when there is an existing available group...")
	group, err := manager.GetAvailableGroup(numNodes)
	assert.NoError(err)
	assert.NotEmpty(group)
	assert.True(group.IsBusy, "the picked group should be busy")

	t.Log("case 2: testing GetAvailableGroup when there is no existing available group but enough free nodes...")
	// add some free nodes
	tu.CreateNodes(numNodes, nodeRepository)
	group, err = manager.GetAvailableGroup(numNodes)
	assert.NoError(err)
	assert.NotEmpty(group)
	assert.True(group.IsBusy, "the picked group should be busy")
}

func TestGetAvailableGroupForBlockchain(t *testing.T) {
	assert := assert.New(t)
	nodeRepository := node.NewRepository()
	groupRepository := group.NewRepository()
	workerStatueUpdater := worker_status_updater.NewUpdater(nodeRepository)
	manager := group.NewManager(nodeRepository, groupRepository, workerStatueUpdater)

	// create enough nodes for testing first
	nodeIDs := tu.CreateNodes(numNodes, nodeRepository)

	// create a new group with the nodes, assuming group.Repository.Create works
	groupToCreate := &group.Group{
		NodeIDs: nodeIDs,
	}

	groupRepository.Create(groupToCreate)
	delegatedNodeID := nodeIDs[0]
	dataNodeIDs := nodeIDs[1:3]

	numAdditionalNodes := 2
	totalNumNodes := numAdditionalNodes + len(dataNodeIDs) + 1
	group, additionalNodeIDs, err := manager.
		GetAvailableGroupForBlockchain(
			dataNodeIDs,
			delegatedNodeID,
			numAdditionalNodes)
	assert.NoError(err)
	assert.NotEmpty(group.ID)
	assert.Equal(numAdditionalNodes, len(additionalNodeIDs))
	assert.Equal(totalNumNodes, len(group.NodeIDs))
	for _, nodeID := range group.NodeIDs {
		node, _ := nodeRepository.GetByID(nodeID)
		assert.True(node.IsGrouped)
	}
	assert.Contains(group.NodeIDs, delegatedNodeID)
	for _, dataNodeID := range dataNodeIDs {
		assert.Contains(group.NodeIDs, dataNodeID)
	}
}
