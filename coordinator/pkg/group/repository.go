package group

import (
	"errors"
	"sync"

	"gitlab.com/arpachain/mpc/coordinator/pkg/util"
)

// Repository for group provides CRUD operations to persist group data
type Repository interface {
	GetAll() ([]Group, error)
	GetByID(id string) (Group, error)
	Create(*Group) (string, error)
	UpdateIsBusy(id string, isBusy bool) error
	UpdateNodeIDs(id string, nodeIDs []string) error
	Remove(id string)
	GetAvailableGroup(size int) (Group, error)
}

// Reference implementation:

type repository struct {
	mu       *sync.Mutex
	groupMap map[string]Group
}

// NewRepository returns an instance of group.Repository
func NewRepository() Repository {
	return &repository{
		mu:       &sync.Mutex{},
		groupMap: map[string]Group{},
	}
}

func (r *repository) GetAll() ([]Group, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	groups := make([]Group, 0)
	for _, group := range r.groupMap {
		groups = append(groups, group)
	}
	return groups, nil
}

// GetByID returns empty group and throws error when id does not exist
func (r *repository) GetByID(id string) (Group, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	if group, exists := r.groupMap[id]; exists {
		return group, nil
	}
	return Group{}, errors.New("group does not exist")
}

func (r *repository) Create(group *Group) (string, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	id, err := util.GenerateID()
	if err != nil {
		return "", err
	}
	if _, exists := r.groupMap[id]; exists {
		return "", errors.New("group with same id already exists")
	}
	r.groupMap[id] = Group{
		ID:      id,
		NodeIDs: group.NodeIDs,
		IsBusy:  false,
	}
	return id, nil
}

func (r *repository) UpdateIsBusy(id string, isBusy bool) error {
	r.mu.Lock()
	defer r.mu.Unlock()
	group, exists := r.groupMap[id]
	if !exists {
		return errors.New("group does not exist")
	}
	group.IsBusy = isBusy
	r.groupMap[id] = group
	return nil
}

func (r *repository) UpdateNodeIDs(id string, nodeIDs []string) error {
	r.mu.Lock()
	defer r.mu.Unlock()
	group, exists := r.groupMap[id]
	if !exists {
		return errors.New("group does not exist")
	}
	if group.IsBusy {
		return errors.New("cannot update a busy group")
	}
	group.NodeIDs = nodeIDs
	r.groupMap[id] = group
	return nil
}

func (r *repository) Remove(id string) {
	r.mu.Lock()
	defer r.mu.Unlock()
	delete(r.groupMap, id)
}

// TODO(bomo): free groups should be released
func (r *repository) GetAvailableGroup(size int) (Group, error) {
	for _, group := range r.groupMap {
		if group.GetSize() == size && !group.IsBusy {
			return group, nil
		}
	}
	return Group{}, errors.New("no available group")
}
