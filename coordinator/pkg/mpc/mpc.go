package mpc

import (
	"gitlab.com/arpachain/mpc/coordinator/api"
)

// MPC as a single MPC execution
type MPC struct {
	ID            string
	NodeIDs       []string
	ResultMap     map[string]arpa_mpc.MpcResult /*key: NodeID*/
	FinishFlagMap map[string]bool               /*key: NodeID*/
}

// IsFinished checks whether all nodes have finished their part of mpc
// to decide whether this mpc instance is finished
func (mpc *MPC) IsFinished() bool {
	for _, isFinished := range mpc.FinishFlagMap {
		if !isFinished {
			return false
		}
	}
	return true
}
