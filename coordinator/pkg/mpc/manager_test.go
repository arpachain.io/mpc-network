package mpc_test

import (
	"testing"

	"gitlab.com/arpachain/mpc/coordinator/api"
	"gitlab.com/arpachain/mpc/coordinator/pkg/mpc"
	tu "gitlab.com/arpachain/mpc/coordinator/test"
	"github.com/stretchr/testify/assert"
)

func TestCreate(t *testing.T) {
	assert := assert.New(t)
	repository := mpc.NewRepository()
	manager := mpc.NewManager(repository)

	fakeNodeIDs := tu.GenerateNodeIDs(numNodes)
	id, err := manager.Create(fakeNodeIDs)
	assert.NoError(err)
	assert.NotEmpty(id)
}

func TestGetByIDs(t *testing.T) {
	assert := assert.New(t)
	repository := mpc.NewRepository()
	manager := mpc.NewManager(repository)

	t.Log("create n MPC")
	ids := tu.CreateMPC(numMPC, repository)

	t.Log("case 1: testing GetByIDs...")
	mpcList, err := manager.GetByIDs(ids)
	assert.NoError(err)
	assert.NotEmpty(mpcList)
	assert.Equal(len(mpcList), numMPC)
}

func TestFinish(t *testing.T) {
	assert := assert.New(t)
	repository := mpc.NewRepository()
	manager := mpc.NewManager(repository)

	t.Log("case 1: testing Finish...")
	fakeNodeIDs := tu.GenerateNodeIDs(numNodes)
	id, _ := repository.Create(tu.GenerateMPC(fakeNodeIDs))
	nodeID := fakeNodeIDs[0]
	mpcResult := &arpa_mpc.MpcResult{
		Data: "fake mpc result data",
	}
	mpcBefore, _ := repository.GetByID(id)
	assert.False(mpcBefore.IsFinished())
	assert.Empty(mpcBefore.ResultMap, "original mpc should have no results")
	err := manager.Finish(id, nodeID, mpcResult)
	assert.NoError(err)
	mpcAfter, _ := repository.GetByID(id)
	assert.True(mpcAfter.FinishFlagMap[nodeID], "mpc for the specific node id should be finished")
}
