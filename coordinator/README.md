# Instructions

## 1. Check dependencies:

Run command: `dep check` under `$GOPATH/src/gitlab.com/arpachain/mpc/coordinator`

## (**optional) If step 1 returns error:

Run command: `dep ensure` under `$GOPATH/src/gitlab.com/arpachain/mpc/coordinator`

## 2. Generate `arpa_mpc.pb.go` file from proto:

Run command: `bash $GOPATH/src/gitlab.com/arpachain/mpc/coordinator/scripts/generate-go-from-proto.sh`

## 3. Build:

Run command: `go build $GOPATH/src/gitlab.com/arpachain/mpc/coordinator/cmd/coordinator/main.go`

## (**optional) Start coordinator without building:

Run command: `go run $GOPATH/src/gitlab.com/arpachain/mpc/coordinator/cmd/coordinator/main.go`

## 3. Run:
Run the binary from (TODO(bomo): Add binary path)
 or run command:
 `go run $GOPATH/src/gitlab.com/arpachain/mpc/coordinator/cmd/coordinator/main.go`
