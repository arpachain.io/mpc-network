package test_util

import (
	"testing"

	"gitlab.com/arpachain/mpc/coordinator/mock/blockchain"
	"gitlab.com/arpachain/mpc/coordinator/mock/certificate"
	"gitlab.com/arpachain/mpc/coordinator/pkg/group"
	"gitlab.com/arpachain/mpc/coordinator/pkg/node"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func TestDemoGroupingLogic(t *testing.T) {
	assert := assert.New(t)
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	generator := mock_certificate.NewMockGenerator(ctrl)
	SetupMockCertificateGenerator(generator)
	mapper := mock_blockchain.NewMockMapper(ctrl)
	mapper.
		EXPECT().
		Add(gomock.Any(), gomock.Any()).
		Return(nil).
		AnyTimes()
	nr := node.NewSequentialRepository()
	gr := group.NewSequentialRepository()
	nm := node.NewManager(nr, mapper, generator)
	gm := group.NewSequentialManager(nr, gr)

	allNodes, err := nr.GetAll()
	assert.NoError(err)
	assert.Empty(allNodes)
	allGroups, err := gr.GetAll()
	assert.NoError(err)
	assert.Empty(allGroups)

	testNodeIDS := make([]string, 0)

	id_1, _, err := nm.Register("127.0.0.1:1000", "1st", []byte("fake"), "0xA2D7CF95645D33006175B78989035C7c9061d3F9")
	assert.NoError(err)
	assert.NotEmpty(id_1)
	testNodeIDS = append(testNodeIDS, id_1)

	id_2, _, err := nm.Register("127.0.0.1:2000", "2nd", []byte("fake"), "0xB2D7CF95645D33006175B78989035C7c9061d3F9")
	assert.NoError(err)
	assert.NotEmpty(id_2)
	testNodeIDS = append(testNodeIDS, id_2)

	id_3, _, err := nm.Register("127.0.0.1:3000", "3rd", []byte("fake"), "0xC2D7CF95645D33006175B78989035C7c9061d3F9")
	assert.NoError(err)
	assert.NotEmpty(id_3)
	testNodeIDS = append(testNodeIDS, id_3)

	id_4, _, err := nm.Register("127.0.0.1:4000", "4th", []byte("fake"), "0xD2D7CF95645D33006175B78989035C7c9061d3F9")
	assert.NoError(err)
	assert.NotEmpty(id_4)
	testNodeIDS = append(testNodeIDS, id_4)

	id_5, _, err := nm.Register("127.0.0.1:5000", "5th", []byte("fake"), "0xE2D7CF95645D33006175B78989035C7c9061d3F9")
	assert.NoError(err)
	assert.NotEmpty(id_5)
	testNodeIDS = append(testNodeIDS, id_5)

	id_6, _, err := nm.Register("127.0.0.1:6000", "6th", []byte("fake"), "0xF2D7CF95645D33006175B78989035C7c9061d3F9")
	assert.NoError(err)
	assert.NotEmpty(id_6)
	testNodeIDS = append(testNodeIDS, id_6)

	id_7, _, err := nm.Register("127.0.0.1:7000", "7th", []byte("fake"), "0xG2D7CF95645D33006175B78989035C7c9061d3F9")
	assert.NoError(err)
	assert.NotEmpty(id_7)
	testNodeIDS = append(testNodeIDS, id_7)

	repoNodes, err := nr.GetAll()
	assert.NoError(err)
	assert.NotEmpty(repoNodes)

	assert.Equal(len(testNodeIDS), len(repoNodes))
	t.Log("Sequence should be the same: registration vs retrieval")
	for i := 0; i < len(repoNodes); i++ {
		assert.Equal(testNodeIDS[i], repoNodes[i].ID)
	}

	t.Log("Initially all nodes should be ungrouped")
	for _, node := range repoNodes {
		assert.False(node.IsGrouped)
	}

	t.Log("Need a group of 3 nodes...")
	firstGroup, err := gm.GetAvailableGroup(3)
	assert.NoError(err)
	assert.NotEmpty(firstGroup)
	assert.True(firstGroup.IsBusy)

	t.Log("First 3 nodes should be grouped")
	assert.Contains(firstGroup.NodeIDs, id_1)
	assert.Contains(firstGroup.NodeIDs, id_2)
	assert.Contains(firstGroup.NodeIDs, id_3)

	t.Log("Need another group of 3 nodes...")
	secondGroup, err := gm.GetAvailableGroup(3)
	assert.NoError(err)
	assert.NotEmpty(secondGroup)
	assert.True(secondGroup.IsBusy)

	t.Log("Next 3 nodes should be grouped")
	assert.Contains(secondGroup.NodeIDs, id_4)
	assert.Contains(secondGroup.NodeIDs, id_5)
	assert.Contains(secondGroup.NodeIDs, id_6)

	t.Log("Ungroup the first group...")
	gm.Ungroup([]string{firstGroup.ID})

	t.Log("First 3 nodes should be ungrouped")
	node_1, _ := nr.GetByID(id_1)
	node_2, _ := nr.GetByID(id_2)
	node_3, _ := nr.GetByID(id_3)
	assert.False(node_1.IsGrouped)
	assert.False(node_2.IsGrouped)
	assert.False(node_3.IsGrouped)

	t.Log("Need another group of 3 nodes...")
	thirdGroup, err := gm.GetAvailableGroup(3)
	assert.NoError(err)
	assert.NotEmpty(thirdGroup)
	assert.True(thirdGroup.IsBusy)

	t.Log("First 3 nodes should be grouped again...")
	assert.Contains(firstGroup.NodeIDs, id_1)
	assert.Contains(firstGroup.NodeIDs, id_2)
	assert.Contains(firstGroup.NodeIDs, id_3)

	t.Log("Ungroup the second group...")
	gm.Ungroup([]string{secondGroup.ID})

	t.Log("Second set of 3 nodes should be ungrouped")
	node_4, _ := nr.GetByID(id_4)
	node_5, _ := nr.GetByID(id_5)
	node_6, _ := nr.GetByID(id_6)
	assert.False(node_4.IsGrouped)
	assert.False(node_5.IsGrouped)
	assert.False(node_6.IsGrouped)

	t.Log("Need another group of 4 nodes...")
	forthGroup, err := gm.GetAvailableGroup(4)
	assert.NoError(err)
	assert.NotEmpty(forthGroup)
	assert.True(forthGroup.IsBusy)

	t.Log("Second set of 4 nodes should be grouped again...")
	assert.Contains(forthGroup.NodeIDs, id_4)
	assert.Contains(forthGroup.NodeIDs, id_5)
	assert.Contains(forthGroup.NodeIDs, id_6)
	assert.Contains(forthGroup.NodeIDs, id_7)
}
