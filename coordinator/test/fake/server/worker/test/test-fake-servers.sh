#!/bin/bash

set -ex

export COORDINATOR_DIR=$GOPATH/src/gitlab.com/arpachain/mpc/coordinator

# Start real coordinator at 127.0.0.1:10000 by default
go run $COORDINATOR_DIR/cmd/coordinator/main.go &

sleep 3

# Start 3 fake workers
go run $COORDINATOR_DIR/test/fake/server/worker/cmd/main.go -waddr 127.0.0.1:6001 \
  -blockchain_addr 0x0000000000000000000000000000000000000001 &
go run $COORDINATOR_DIR/test/fake/server/worker/cmd/main.go -waddr 127.0.0.1:6002 \
  -blockchain_addr 0x0000000000000000000000000000000000000002 &
go run $COORDINATOR_DIR/test/fake/server/worker/cmd/main.go -waddr 127.0.0.1:6003 \
  -blockchain_addr 0x0000000000000000000000000000000000000003 &

sleep 3

# Use caller to call
go run $COORDINATOR_DIR/caller/main.go \
&& \

# Clean up all the processes
fuser -k 10000/tcp || true
fuser -k 6001/tcp || true
fuser -k 6002/tcp || true
fuser -k 6003/tcp || true
