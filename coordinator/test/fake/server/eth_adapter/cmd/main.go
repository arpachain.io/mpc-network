package main

import (
	"context"
	"flag"
	"log"
	"net"

	pb "gitlab.com/arpachain/mpc/coordinator/api"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var (
	address    = flag.String("address", "127.0.0.1:40000", "The ip address and port number of the fake blockchain_adapter server")
	bufferSize = flag.Int("bufferSize", 6, "the buffer size of the result channel")
)

func main() {
	flag.Parse()
	lis, err := net.Listen("tcp", *address)

	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	log.Printf("Server listening on : %v", *address)
	s := grpc.NewServer()

	blochchainAdapterServer := NewFakeBlochchainAdapterServer(*bufferSize)

	pb.RegisterBlochchainAdapterServer(s, blochchainAdapterServer)

	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}

type FakeBlochchainAdapterServer struct {
	ResultChan chan *pb.ReportMpcResultRequest
}

func NewFakeBlochchainAdapterServer(bufferSize int) *FakeBlochchainAdapterServer {
	return &FakeBlochchainAdapterServer{
		ResultChan: make(chan *pb.ReportMpcResultRequest, bufferSize),
	}
}

func (s *FakeBlochchainAdapterServer) ReportMpcResult(
	ctx context.Context,
	req *pb.ReportMpcResultRequest,
) (*pb.ReportMpcResultResponse, error) {
	s.ResultChan <- req
	return &pb.ReportMpcResultResponse{
		Success: true,
	}, nil
}

func (s *FakeBlochchainAdapterServer) UpdateWorkerList(
	ctx context.Context,
	req *pb.UpdateWorkerListRequest,
) (*pb.UpdateWorkerListResponse, error) {
	return &pb.UpdateWorkerListResponse{
		Success: true,
	}, nil
}
