#!/bin/bash

set -ex
PASSWORD=defaultPassword
OUTPUT_PATH=$GOPATH/src/gitlab.com/arpachain/mpc/coordinator/pkg/certificate/test_certificates
DAYS=730

if [ "$(uname)" == "Darwin" ]; then
    # Mac OS X platform
    SUBJECT="/C=CN/ST=Beijing/L=Chaoyang/O=ARPA/CN=arpachain.io"        
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    # GNU/Linux platform
    SUBJECT="/C=CN/ST=Beijing/L=Chaoyang/O=ARPA/CN=arpachain.io"
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
    # 32 bits Windows NT platform
    SUBJECT="//C=CN\ST=Beijing\L=Chaoyang\O=ARPA\CN=arpachain.io"
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW64_NT" ]; then
    # 64 bits Windows NT platform
    SUBJECT="//C=CN\ST=Beijing\L=Chaoyang\O=ARPA\CN=arpachain.io"
fi

# Create CA (self-signed):

# Generate CA private key:
openssl genrsa \
  -out $OUTPUT_PATH/ca.key 4096
# Generate CA certificate:
openssl req \
  -subj $SUBJECT \
  -x509 \
  -days $DAYS \
  -key $OUTPUT_PATH/ca.key \
  -out $OUTPUT_PATH/ca.crt


# Client(node) needs to generate CSR:

# Generate client-side private key:
openssl genrsa \
  -out $OUTPUT_PATH/test_node_1.key 4096
# Generate Certificate Sign Request (CSR):
openssl req \
  -new \
  -subj $SUBJECT \
  -key $OUTPUT_PATH/test_node_1.key \
  -out $OUTPUT_PATH/test_node_1.csr