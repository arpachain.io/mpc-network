module gitlab.com/arpachain/mpc/coordinator

go 1.13

require (
	github.com/golang/mock v1.3.1
	github.com/golang/protobuf v1.3.2
	github.com/processout/grpc-go-pool v1.2.1
	github.com/rs/xid v1.2.1
	github.com/stretchr/testify v1.4.0
	golang.org/x/net v0.0.0-20190926025831-c00fd9afed17
	google.golang.org/grpc v1.24.0
)
