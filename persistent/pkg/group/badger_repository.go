package group

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"

	"gitlab.com/arpachain/mpc/coordinator/pkg/group"
	"gitlab.com/arpachain/mpc/persistent/pkg/db"
	"github.com/dgraph-io/badger"
)

const (
	keyPrefix = "group_"
)

// BadgerRepository implements interface:
// gitlab.com/arpachain/mpc/coordinator/pkg/group/Repository
type badgerRepository struct {
	db *badger.DB
}

// NewBadgerRepository returns a BadgerDB-based implementation of interface:
// gitlab.com/arpachain/mpc/coordinator/pkg/group/Repository
func NewBadgerRepository(db *badger.DB) group.Repository {
	return &badgerRepository{
		db: db,
	}
}

func (r *badgerRepository) GetAll() ([]group.Group, error) {
	groups := make([]group.Group, 0)
	objects, err := db_utils.GetAllObjects(r.db, keyPrefix, &group.Group{})
	if err != nil {
		return nil, err
	}
	for _, obj := range objects {
		g := obj.(*group.Group)
		groups = append(groups, *g)
	}
	return groups, nil
}

// GetByID returns empty group and throws error when id does not exist
func (r *badgerRepository) GetByID(id string) (group.Group, error) {
	var g group.Group
	exists, err := db_utils.GetObject(r.db, keyPrefix, id, &g)
	if err != nil {
		return group.Group{}, err
	}
	if !exists {
		return group.Group{}, fmt.Errorf("the group with id: %v does not exist", id)
	}
	return g, nil
}

func (r *badgerRepository) Create(group *group.Group) (string, error) {
	groupBytes, err := json.Marshal(group)
	if err != nil {
		return "", err
	}
	err = db_utils.CreateObject(r.db, keyPrefix, group.ID, groupBytes)
	if err != nil {
		return "", err
	}
	return group.ID, nil
}

func (r *badgerRepository) UpdateIsBusy(id string, isBusy bool) error {
	return r.readAndUpdate(id,
		func(oldGroup *group.Group) (*group.Group, error) {
			if oldGroup.IsBusy == isBusy {
				return nil, fmt.Errorf("the group is already in the same state: IsBusy = %v", isBusy)
			}
			oldGroup.IsBusy = isBusy
			return oldGroup, nil
		})
}

func (r *badgerRepository) UpdateNodeIDs(id string, nodeIDs []string) error {
	return r.readAndUpdate(id,
		func(oldGroup *group.Group) (*group.Group, error) {
			if len(nodeIDs) == 0 {
				return nil, errors.New("nodeIDs cannot be empty")
			}
			oldGroup.NodeIDs = nodeIDs
			return oldGroup, nil
		})
}

func (r *badgerRepository) Remove(id string) {
	err := db_utils.DeleteObject(r.db, keyPrefix, id)
	// TODO(bomo) need to change the interface behavior for error handling
	if err != nil {
		log.Printf("deleting group : %v failed for reason: %v", id, err)
	}
}

// TODO(bomo): free groups should be released
func (r *badgerRepository) GetAvailableGroup(size int) (group.Group, error) {
	groups, err := r.GetAll()
	if err != nil {
		return group.Group{}, err
	}
	for _, group := range groups {
		if group.GetSize() == size && !group.IsBusy {
			return group, nil
		}
	}
	return group.Group{}, errors.New("no available group")
}

func (r *badgerRepository) readAndUpdate(
	id string,
	updateFn func(*group.Group) (*group.Group, error),
) error {
	err := r.db.Update(func(txn *badger.Txn) error {
		var oldGroup group.Group
		item, err := txn.Get([]byte(keyPrefix + id))
		if err != nil {
			return err
		}
		err = item.Value(func(v []byte) error {
			err := json.Unmarshal(v, &oldGroup)
			return err
		})
		if err != nil {
			return err
		}
		newGroup, err := updateFn(&oldGroup)
		if err != nil {
			return err
		}
		groupBytes, err := json.Marshal(newGroup)
		if err != nil {
			return err
		}
		return txn.Set([]byte(keyPrefix+newGroup.ID), groupBytes)
	})
	return err
}
