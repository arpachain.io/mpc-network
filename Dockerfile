FROM debian:latest
EXPOSE 4000-9000
ENV GOROOT=/usr/local/go
ENV GOPATH=/root/go
ENV PATH=$PATH:$GOPATH/bin:$GOROOT/bin:/root/protoc-3.6.1/bin
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global
ENV PATH=$PATH:/home/node/.npm-global/bin
ENV PATH=$PATH:/usr/bin
ARG DEBIAN_FRONTEND=noninteractive

# install required tools, gcc-7, and g++-7
RUN set -ex \
  && apt-get update \
  && apt-get --yes install apt-utils \
  && apt-get --yes upgrade \
  && apt-get --yes install unzip \
  && apt-get --yes install wget \
  && apt-get --yes install curl \
  && apt-get --yes install bzip2 \
  && apt-get --yes install git \
  && apt-get --yes install build-essential autoconf libtool pkg-config \
  && apt-get --yes install software-properties-common \
  && add-apt-repository 'deb http://deb.debian.org/debian buster main' \
  && apt-get update \
  && apt-get --yes install -t buster gcc-7 \
  && apt-get --yes install -t buster g++-7 \
  && rm /usr/bin/cpp /usr/bin/gcc /usr/bin/g++ \
  && ln -s /usr/bin/cpp-7 /usr/bin/cpp \
  && ln -s /usr/bin/gcc-7 /usr/bin/gcc \
  && ln -s /usr/bin/g++-7 /usr/bin/g++ \
  && gcc -v

# install gRPC and protobuf
RUN set -ex \
  && apt-get update \
  && apt-get --yes install libgflags-dev libgtest-dev \
  && apt-get --yes install clang libc++-dev \
  && cd \
  && git clone -b v1.16.0 https://github.com/grpc/grpc \
  && cd grpc \
  && git submodule update --init \
  && make && make install \
  && make grpc_cli \
  && ln -s /root/grpc/bins/opt/grpc_cli /usr/bin/grpc_cli \
  && cd third_party/protobuf \
  && make install

# install utils including glog and gflag 
RUN set -ex \
  && apt-get update \
  && apt-get --yes install cmake \
  && cd \
  && git clone https://github.com/gflags/gflags \
  && cd gflags \
  && mkdir build \
  && cd build \
  && cmake -DCMAKE_INSTALL_PREFIX=/usr/local -DBUILD_SHARED_LIBS=ON -G"Unix Makefiles" .. \
  && make -j4 && make install && ldconfig \
  && cd \
  && git clone https://github.com/google/glog \
  && cd glog \
  && mkdir build \
  && cd build \
  && cmake -D BUILD_gflags_LIBS=ON -D BUILD_SHARED_LIBS=ON -D BUILD_gflags_nothreads_LIBS=ON -D GFLAGS_NAMESPACE=ON .. \
  && make -j4 && make install


# install go, go tools, dep, and protoc-gen-go plugin
RUN set -ex \
  && apt-get update \
  && mkdir -p /usr/local/ \
  && cd \
  && wget https://dl.google.com/go/go1.11.2.linux-amd64.tar.gz \
  && tar -C /usr/local -xzf go1.11.2.linux-amd64.tar.gz \
  && go version \
  && go env \
  && go tool \
  && go get -u -v golang.org/x/tools/... \
  && go get -u -v google.golang.org/grpc \
  && cd \
  && wget https://github.com/protocolbuffers/protobuf/releases/download/v3.6.1/protoc-3.6.1-linux-x86_64.zip \
  && unzip protoc-3.6.1-linux-x86_64.zip -d protoc-3.6.1 \
  && protoc --version \
  && go get -u -v github.com/golang/protobuf/protoc-gen-go \
  && go get -u -v github.com/golang/dep/cmd/dep \
  && dep version \
  && go get -u -v github.com/golang/mock/gomock \
  && go install github.com/golang/mock/mockgen \
  && go get -u -v github.com/processout/grpc-go-pool

# install prerequisite for SCALE-MAMBA(ARPA-Fork) (yasm, mpir, gmpy2, openssl, boost-filesystem)
RUN set -ex \
  && apt-get update \
  && apt-get install libboost-filesystem-dev --yes \
  && apt-get --yes install yasm \
  && cd \
  && wget http://mpir.org/mpir-3.0.0.tar.bz2 \
  && tar -xvf mpir-3.0.0.tar.bz2 \
  && cd mpir-3.0.0 \
  && ./configure --enable-cxx \
  && make && make check && make install \
  && apt-get --yes install python-dev \
  && apt-get --yes install python-gmpy2 \
  && cd \
  && wget https://www.openssl.org/source/openssl-1.1.1.tar.gz \
  && tar -xvf openssl-1.1.1.tar.gz \
  && cd openssl-1.1.1 \
  && ./config && make && make install \
  && cd \
  && echo '/usr/local/lib' >> /etc/ld.so.conf.d/libs.conf \
  && ldconfig

# install nodejs, npm, truffle, ganache-cli, and geth
RUN set -ex \
  && cd \
  && apt-get update \
  && curl -sL https://deb.nodesource.com/setup_11.x | bash - \
  && apt-get --yes install nodejs \
  && node --version \
  && npm --version \
  && npm install --global --unsafe-perm truffle@v5.0.1 \
  && truffle version \
  && npm install --global --unsafe-perm ganache-cli \
  && ganache-cli --version \
  && npm install --global --unsafe-perm openzeppelin-solidity@v2.1.0-rc.2 \
  && go get -d -v  github.com/ethereum/go-ethereum \
  && cd $GOPATH/src/github.com/ethereum/go-ethereum \
  && go install ./... \
  && make \
  && make devtools \
  && geth version \
  && curl -o /usr/bin/solc -fL https://github.com/ethereum/solidity/releases/download/v0.5.4/solc-static-linux \
  && chmod u+x /usr/bin/solc \
  && solc --version
