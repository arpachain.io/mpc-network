package main

import (
	"flag"
	"log"
	"net"
	"os"

	pb "gitlab.com/arpachain/mpc/datagateway/api"
	"gitlab.com/arpachain/mpc/datagateway/internal/data"
	"gitlab.com/arpachain/mpc/datagateway/internal/server"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var (
	address        = flag.String("address", "127.0.0.1:11000", "The ip address and port number of the datagateway server")
	dataSetName    = flag.String("dataset", "Test1", "The name of the data set.")
	pathToDataFile = flag.String(
		"pathToDataFile",
		os.Getenv("GOPATH")+"/src/gitlab.com/arpachain/mpc/datagateway/internal/data/test_data/mpc_data.json",
		"The path to the data file.")
)

func main() {
	flag.Parse()
	lis, err := net.Listen("tcp", *address)

	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	log.Printf("Server listening on : %v", *address)
	s := grpc.NewServer()

	repository := data.NewRepository(*pathToDataFile)
	server := server.NewDataGatewayServer(repository, *dataSetName)

	pb.RegisterDataGatewayServer(s, server)

	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}
