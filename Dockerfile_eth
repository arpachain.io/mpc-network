# TODO(bomo): Need to move this to eth once it's moved to an independent directory
FROM debian:latest
EXPOSE 4000-9000
ENV GOROOT=/usr/local/go
ENV GOPATH=/root/go
ENV PATH=$PATH:$GOPATH/bin:$GOROOT/bin:/root/protoc-3.6.1/bin
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global
ENV PATH=$PATH:/home/node/.npm-global/bin

# install required tools
RUN set -ex \
  && apt-get update \
  && apt-get --yes upgrade \
  && apt-get --yes install unzip \
  && apt-get --yes install wget \
  && apt-get --yes install curl \
  && apt-get --yes install bzip2 \
  && apt-get --yes install git \
  && apt-get --yes install build-essential autoconf libtool pkg-config \
  && apt-get --yes install software-properties-common

# install go, go tools, dep, and protoc-gen-go plugin
RUN set -ex \
  && apt-get update \
  && mkdir -p /usr/local/ \
  && cd \
  && wget https://dl.google.com/go/go1.11.2.linux-amd64.tar.gz \
  && tar -C /usr/local -xzf go1.11.2.linux-amd64.tar.gz \
  && go version \
  && go env \
  && go tool \
  && go get -u -v google.golang.org/grpc \
  && cd \
  && wget https://github.com/protocolbuffers/protobuf/releases/download/v3.6.1/protoc-3.6.1-linux-x86_64.zip \
  && unzip protoc-3.6.1-linux-x86_64.zip -d protoc-3.6.1 \
  && protoc --version \
  && go get -u -v github.com/golang/protobuf/protoc-gen-go \
  && go get -u -v github.com/golang/dep/cmd/dep \
  && dep version

# install nodejs, npm, truffle, ganache-cli, and geth
RUN set -ex\
  && cd \
  && apt-get update \
  && curl -sL https://deb.nodesource.com/setup_11.x | bash - \
  && apt-get --yes install nodejs \
  && node --version \
  && npm --version \
  && npm install --global --unsafe-perm truffle@v5.0.1 \
  && truffle version \
  && npm install --global --unsafe-perm ganache-cli \
  && ganache-cli --version \
  && npm install --global --unsafe-perm openzeppelin-solidity@v2.1.0-rc.2 \
  && go get -d -v  github.com/ethereum/go-ethereum \
  && cd $GOPATH/src/github.com/ethereum/go-ethereum \
  && go install ./... \
  && geth version
