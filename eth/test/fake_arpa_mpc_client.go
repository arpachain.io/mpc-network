package test

import (
	"context"

	pb "gitlab.com/arpachain/mpc/eth/api"
	"google.golang.org/grpc"
)

// TODO(John): use mock
type fakeArpaMPCClient struct{}

func NewFakeArpaMPCClient() pb.ArpaMpcClient {
	return &fakeArpaMPCClient{}
}

func (s *fakeArpaMPCClient) RunMpc(
	ctx context.Context,
	request *pb.RunMpcRequest,
	opts ...grpc.CallOption,
) (*pb.RunMpcResponse, error) {
	mpcResult := &pb.MpcResult{
		Data: "mpc_result_from_fake_ArpaMpcServer",
	}
	mpcResults := make([]*pb.MpcResult, 0)
	mpcResults = append(mpcResults, mpcResult)
	response := &pb.RunMpcResponse{
		MpcFinalResult: mpcResults,
	}
	return response, nil
}

func (s *fakeArpaMPCClient) RunMpcForBlockchain(
	ctx context.Context,
	in *pb.RunMpcForBlockchainRequest,
	opts ...grpc.CallOption,
) (*pb.RunMpcForBlockchainResponse, error) {
	additionalWorkers := []string{
		"0x709cefa1ef25336a3e625e63f4ab886c267a184d",
		"0x15a306fac0c6931ab93dd435d11b0d93b1326e0d",
	}
	response := &pb.RunMpcForBlockchainResponse{
		MpcId:                        "Test MpcId", // TODO(John): change every time
		AdditionalWorkerBlockchainAddresses: additionalWorkers,
	}
	return response, nil
}
