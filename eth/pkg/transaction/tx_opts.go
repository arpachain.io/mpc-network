package transaction

import (
	"context"
	"crypto/ecdsa"
	"errors"
	"math/big"

	"gitlab.com/arpachain/mpc/eth/pkg/abstract"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/crypto"
)

const defaultGasLimit uint64 = 3000000

// generate parameters that used for a transaction
type OptsGenerator interface {
	GenerateDefaultOpts(ctx context.Context, value *big.Int) (*bind.TransactOpts, error)
	GenerateOpts(ctx context.Context, value *big.Int, gasLimit uint64) (*bind.TransactOpts, error)
}

// Reference implementation:
type optsGenerator struct {
	privateKey *ecdsa.PrivateKey
	client     abstract.ClientForTxOptsGenerator
}

// NewOptsGenerator returns an instance of eth.OptsGenerator
func NewOptsGenerator(sk *ecdsa.PrivateKey, c abstract.ClientForTxOptsGenerator) OptsGenerator {
	return &optsGenerator{
		privateKey: sk,
		client:     c,
	}
}

func (g *optsGenerator) GenerateDefaultOpts(ctx context.Context, value *big.Int) (*bind.TransactOpts, error) {
	publicKey := g.privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		return nil, errors.New("error casting public key to ECDSA")
	}

	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)

	nonce, err := g.client.PendingNonceAt(ctx, fromAddress)
	if err != nil {
		return nil, err
	}

	gasPrice, err := g.client.SuggestGasPrice(ctx)
	if err != nil {
		return nil, err
	}

	opts := bind.NewKeyedTransactor(g.privateKey)
	opts.Nonce = big.NewInt(int64(nonce))
	opts.Value = value              // in wei
	opts.GasLimit = defaultGasLimit // in units
	opts.GasPrice = gasPrice
	return opts, nil
}

func (g *optsGenerator) GenerateOpts(ctx context.Context, value *big.Int, gasLimit uint64) (*bind.TransactOpts, error) {
	publicKey := g.privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		return nil, errors.New("error casting public key to ECDSA")
	}

	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)

	nonce, err := g.client.PendingNonceAt(ctx, fromAddress)
	if err != nil {
		return nil, err
	}

	gasPrice, err := g.client.SuggestGasPrice(context.Background())
	if err != nil {
		return nil, err
	}

	opts := bind.NewKeyedTransactor(g.privateKey)
	opts.Nonce = big.NewInt(int64(nonce))
	opts.Value = value       // in wei
	opts.GasLimit = gasLimit // in units
	opts.GasPrice = gasPrice
	return opts, nil

}
