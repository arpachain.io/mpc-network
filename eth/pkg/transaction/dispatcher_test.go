package transaction_test

import (
	"context"
	"errors"
	"math/big"
	"testing"
	"time"

	mock_transaction "gitlab.com/arpachain/mpc/eth/mock/transaction"
	"gitlab.com/arpachain/mpc/eth/pkg/abstract"
	"gitlab.com/arpachain/mpc/eth/pkg/transaction"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

const numConfirmations = 5

func TestSend(t *testing.T) {
	assert := assert.New(t)
	ctrl := gomock.NewController(t)
	mockOptsGenerator := mock_transaction.NewMockOptsGenerator(ctrl)
	setMockOptsGeneratorBehavior_Valid(mockOptsGenerator)
	defer ctrl.Finish()

	fakeClient := NewFakeUnstableClientForTxConfirmation(2 * numConfirmations)
	dispatcher := transaction.NewDispatcher(
		fakeClient,
		mockOptsGenerator,
		numConfirmations,   /* required numConfirmations */
		time.Millisecond,   /* pollingIntervalForMining */
		5*time.Millisecond, /* pollingIntervalForConfirmation */
	)

	ctx := context.Background()

	called := false
	callContract := func(opts *bind.TransactOpts) (*types.Transaction, error) {
		called = true
		return &types.Transaction{}, nil
	}

	err := dispatcher.Send(ctx, big.NewInt(0), callContract)
	assert.NoError(err)
	assert.True(called)
}

// Every possible retriable function is also tested
func TestUnstableSend(t *testing.T) {
	assert := assert.New(t)
	ctrl := gomock.NewController(t)
	mockOptsGenerator := mock_transaction.NewMockOptsGenerator(ctrl)
	setMockOptsGeneratorBehavior_Unstable(mockOptsGenerator)
	defer ctrl.Finish()

	fakeClient := NewFakeUnstableClientForTxConfirmation(2 * numConfirmations)
	dispatcher := transaction.NewDispatcher(
		fakeClient,
		mockOptsGenerator,
		numConfirmations,   /* required numConfirmations */
		time.Millisecond,   /* pollingIntervalForMining */
		5*time.Millisecond, /* pollingIntervalForConfirmation */
	)

	ctx := context.Background()

	called := false
	calledOnce := false
	// the first call will fail to simulate an unstable case
	callContract := func(opts *bind.TransactOpts) (*types.Transaction, error) {
		if calledOnce {
			called = true
			return &types.Transaction{}, nil
		} else {
			calledOnce = true
			return nil, errors.New("Unstable call to callContract")
		}
	}

	err := dispatcher.Send(ctx, big.NewInt(0), callContract)
	assert.NoError(err)
	assert.True(called)
}

// Send first a slow tx, then a fast tx, test the slow one does block the fast one
func TestSendDoBlock(t *testing.T) {
	assert := assert.New(t)
	ctrl := gomock.NewController(t)
	mockOptsGenerator := mock_transaction.NewMockOptsGenerator(ctrl)
	setMockOptsGeneratorBehavior_Valid(mockOptsGenerator)
	defer ctrl.Finish()

	fakeClient := NewFakeClientForTxConfirmation(2 * numConfirmations)
	dispatcher := transaction.NewDispatcher(
		fakeClient,
		mockOptsGenerator,
		numConfirmations, /* required numConfirmations */
		time.Millisecond, /* pollingIntervalForMining */
		time.Millisecond, /* pollingIntervalForConfirmation */
	)

	ctx := context.Background()

	called1 := false
	callContract1 := func(opts *bind.TransactOpts) (*types.Transaction, error) {
		called1 = true
		time.Sleep(100 * time.Millisecond) // simulate a slow tx
		return &types.Transaction{}, nil
	}

	errChan := make(chan error)
	resultChan := make(chan bool)

	go func() {
		err := dispatcher.Send(ctx, big.NewInt(0), callContract1)
		assert.NoError(err)
		resultChan <- true
	}()

	// wait so that the first tx is called
	time.Sleep(10 * time.Millisecond)

	called2 := false
	// call to be blocked
	callContract2 := func(opts *bind.TransactOpts) (*types.Transaction, error) {
		called2 = true
		return &types.Transaction{}, nil
	}

	go func() {
		errChan <- dispatcher.Send(ctx, big.NewInt(0), callContract2)
	}()
	select {
	case <-time.After(200 * time.Millisecond):
		t.Fatal("Did not receive the return from the first call in a resonable time!")
	case <-resultChan:
	case <-errChan:
		t.Fatal("The second call is not blocked!")
	}

	select {
	case <-time.After(100 * time.Millisecond):
		t.Fatal("Did not receive the return from the second call in a reasonable time!")
	case err := <-errChan:
		assert.NoError(err)
	}
	assert.True(called1)
	assert.True(called2)
}

// each time there is a query to this client,
// current block is added by numBlockPerCommit
type fakeClientForTxConfirmation struct {
	numBlockPerCommit, fakeBlockNumber int64
}

func NewFakeClientForTxConfirmation(numBlockPerCommit int64) abstract.ClientForTxConfirmation {
	return &fakeClientForTxConfirmation{
		numBlockPerCommit: numBlockPerCommit,
		fakeBlockNumber:   1, /* init block number at 1 */
	}
}

func (c *fakeClientForTxConfirmation) HeaderByNumber(ctx context.Context, number *big.Int) (*types.Header, error) {
	header := &types.Header{
		Number: big.NewInt(c.fakeBlockNumber),
	}
	c.fakeBlockNumber = c.fakeBlockNumber + c.numBlockPerCommit
	return header, nil
}

func (c *fakeClientForTxConfirmation) TransactionReceipt(ctx context.Context, txHash common.Hash) (*types.Receipt, error) {
	return &types.Receipt{
		Status: types.ReceiptStatusSuccessful,
	}, nil
}

// this is same as fakeClientForTxConfirmation
// except that each function will fail 1/2 times
type fakeUnstableClientForTxConfirmation struct {
	numBlockPerCommit, fakeBlockNumber             int64
	calledHeaderByNumber, calledTransactionReceipt bool
}

func NewFakeUnstableClientForTxConfirmation(numBlockPerCommit int64) abstract.ClientForTxConfirmation {
	return &fakeUnstableClientForTxConfirmation{
		numBlockPerCommit:        numBlockPerCommit,
		fakeBlockNumber:          1, /* init block number at 1 */
		calledHeaderByNumber:     false,
		calledTransactionReceipt: false,
	}
}

func (c *fakeUnstableClientForTxConfirmation) HeaderByNumber(ctx context.Context, number *big.Int) (*types.Header, error) {
	if c.calledHeaderByNumber {
		c.calledHeaderByNumber = false
		header := &types.Header{
			Number: big.NewInt(c.fakeBlockNumber),
		}
		c.fakeBlockNumber = c.fakeBlockNumber + c.numBlockPerCommit
		return header, nil
	} else {
		c.calledHeaderByNumber = true
		return nil, errors.New("Unstable failure to HeaderByNumber")
	}
}

func (c *fakeUnstableClientForTxConfirmation) TransactionReceipt(ctx context.Context, txHash common.Hash) (*types.Receipt, error) {
	if c.calledTransactionReceipt {
		c.calledTransactionReceipt = false
		return &types.Receipt{
			Status: types.ReceiptStatusSuccessful,
		}, nil
	} else {
		c.calledTransactionReceipt = true
		return nil, errors.New("Unstable failure to TransactionReceipt")
	}
}

func setMockOptsGeneratorBehavior_Valid(g *mock_transaction.MockOptsGenerator) {
	g.
		EXPECT().
		GenerateDefaultOpts(gomock.Any(), gomock.Any()).
		Return(nil, nil).
		AnyTimes()

	g.
		EXPECT().
		GenerateOpts(gomock.Any(), gomock.Any(), gomock.Any()).
		Return(nil, nil).
		AnyTimes()
}

func setMockOptsGeneratorBehavior_Unstable(g *mock_transaction.MockOptsGenerator) {
	g.
		EXPECT().
		GenerateDefaultOpts(gomock.Any(), gomock.Any()).
		Return(nil, errors.New("Unstable generation of Opts"))
	g.
		EXPECT().
		GenerateDefaultOpts(gomock.Any(), gomock.Any()).
		Return(nil, nil)

	g.
		EXPECT().
		GenerateOpts(gomock.Any(), gomock.Any(), gomock.Any()).
		Return(nil, nil).
		AnyTimes()
}
