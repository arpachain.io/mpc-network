package transaction

import (
	"encoding/json"
	"log"

	db_utils "gitlab.com/arpachain/mpc/persistent/pkg/db"
	"github.com/dgraph-io/badger"
	"github.com/ethereum/go-ethereum/common"
)

const (
	KeyPrefix = "ethtx_"
)

var (
	trueBytes []byte
)

func init() {
	var err error
	trueBytes, err = json.Marshal(true)
	if err != nil {
		log.Fatal(err)
	}
}

type Repository interface {
	Insert(hash common.Hash) error
	HasKey(hash common.Hash) (bool, error)
}

// a BadgerDB-based implementation
type badgerRepository struct {
	db *badger.DB
}

// NewBadgerRepository returns a BadgerDB-based implementation
func NewBadgerRepository(db *badger.DB) Repository {
	return &badgerRepository{
		db: db,
	}
}

func (r *badgerRepository) HasKey(hash common.Hash) (bool, error) {
	var b bool
	h := hash.String()
	exists, err := db_utils.GetObject(r.db, KeyPrefix, h, &b)
	if err != nil {
		return false, err
	}
	return exists, nil
}

func (r *badgerRepository) Insert(hash common.Hash) error {
	h := hash.String()
	err := db_utils.CreateObject(r.db, KeyPrefix, h, trueBytes)
	if err != nil {
		return err
	}
	return nil
}
