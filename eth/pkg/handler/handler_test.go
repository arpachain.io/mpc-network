package handler_test

import (
	"errors"
	"log"
	"math/big"
	"testing"
	"time"

	pb "gitlab.com/arpachain/mpc/eth/api"
	mock_arpa_mpc "gitlab.com/arpachain/mpc/eth/mock/api"
	mock_parser "gitlab.com/arpachain/mpc/eth/mock/parser"
	mock_request "gitlab.com/arpachain/mpc/eth/mock/request"
	mock_worker_list "gitlab.com/arpachain/mpc/eth/mock/worker_list"
	"gitlab.com/arpachain/mpc/eth/pkg/abstract"
	"gitlab.com/arpachain/mpc/eth/pkg/handler"
	"gitlab.com/arpachain/mpc/eth/pkg/model"
	"gitlab.com/arpachain/mpc/eth/test/fake"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

const (
	Coordinator int = iota
	DataProviderWorker
	User
	Worker1
	Worker2
	DelegatedWorker

	computationStarted int = iota
	resultPosted
	workerListUpdated
)

var (
	workerListStr = []string{
		"0x41aaade8bcfcdfa4526ea4cd50b695226acba37676fbbcb94b7f56c6499cad3d",
		"0x167073e599b7e8142183327e5e9c5561ee4a4179dd3ad2683f903089af7c2852",
		"0xa9dacc161bf9b8005167d1d61fa73b9d2dcf644c50f6dcd3fa97764742b2d44c",
		"0xd4bdc67ee0a4cf059449726b0842aacb54301ba5d2ed0473b0872491c4c20bb2",
	}
	workerList []common.Address
)

func init() {
	workerList = make([]common.Address, len(workerListStr))
	for index, worker := range workerListStr {
		workerList[index] = common.HexToAddress(worker)
	}
}

// HandleComputationStartedEvent is also tested
func TestHandleRequestReceivedEvent(t *testing.T) {
	assert := assert.New(t)
	eventChan := make(chan int)
	instance := NewFakeContract(eventChan, assert)
	ctrl := gomock.NewController(t)
	handler := createTestHandler(instance, ctrl, false)
	defer ctrl.Finish()

	log.Println("Receiving MPC request...")
	event := model.Event{}
	requestReceivedChan := make(chan bool)
	// send a prepared event to the handler
	go func() {
		handler.HandleRequestReceivedEvent(&event)
		requestReceivedChan <- true
	}()
	select {
	case event := <-eventChan:
		assert.Equal(computationStarted, event)
	case <-time.After(5 * time.Millisecond):
		t.Fatalf("No response from StartComputation in a reasonable amount of time")
	}
	event = model.Event{}
	log.Println("Receiving mock ComputationStarted")
	handler.HandleComputationStartedEvent(&event)

	select {
	case requestRecieved := <-requestReceivedChan:
		assert.True(requestRecieved)
	case <-time.After(5 * time.Millisecond):
		t.Fatalf("HandleRequestReceivedEvent did not finish in a reasonable time")
	}

}

// Fail and retry for RunMpcForBlockchain is tested
func TestUnstableHandleRequestReceivedEvent(t *testing.T) {
	assert := assert.New(t)
	eventChan := make(chan int)
	instance := NewFakeContract(eventChan, assert)
	ctrl := gomock.NewController(t)
	handler := createTestHandler(instance, ctrl, true)
	defer ctrl.Finish()

	log.Println("Receiving MPC request...")
	event := model.Event{}
	requestReceivedChan := make(chan bool)
	// send a prepared event to the handler
	go func() {
		handler.HandleRequestReceivedEvent(&event)
		requestReceivedChan <- true
	}()
	select {
	case event := <-eventChan:
		assert.Equal(computationStarted, event)
	case <-time.After(2 * time.Second):
		t.Fatalf("No response from StartComputation in a reasonable amount of time")
	}
	event = model.Event{}
	log.Println("Receiving mock ComputationStarted")
	handler.HandleComputationStartedEvent(&event)

	select {
	case requestRecieved := <-requestReceivedChan:
		assert.True(requestRecieved)
	case <-time.After(5 * time.Millisecond):
		t.Fatalf("HandleRequestReceivedEvent did not finish in a reasonable time")
	}

}

// HandleResultPostedEvent is also tested
func TestHandleFinishMpcBlockchain(t *testing.T) {
	assert := assert.New(t)
	eventChan := make(chan int)
	instance := NewFakeContract(eventChan, assert)
	ctrl := gomock.NewController(t)
	handler := createTestHandler(instance, ctrl, false)
	defer ctrl.Finish()

	handledChan := make(chan bool)
	go func() {
		mpcResult := &pb.MpcResult{
			Data: "mpc_result",
		}
		mpcResults := make([]*pb.MpcResult, 0)
		mpcResults = append(mpcResults, mpcResult)

		req := &pb.ReportMpcResultRequest{
			MpcId:  "Test MpcId",
			Result: mpcResults,
		}
		handler.HandleReportMpcResultBlockchain(req)
		handledChan <- true
	}()
	select {
	case event := <-eventChan:
		assert.Equal(resultPosted, event)
	case <-time.After(5 * time.Millisecond):
		t.Fatalf("No response from PostResult in a reasonable amount of time")
	}
	event := model.Event{}
	log.Println("Receiving mock ResultPostedEvent")
	handler.HandleResultPostedEvent(&event)
	select {
	case handled := <-handledChan:
		assert.True(handled)
	case <-time.After(5 * time.Millisecond):
		t.Fatalf("HandleReportMpcResultBlockchain did not finish in a reasonable time")
	}

}

func TestHandleUpdateWorkerListBlockchain(t *testing.T) {
	assert := assert.New(t)
	eventChan := make(chan int)
	instance := NewFakeContract(eventChan, assert)
	ctrl := gomock.NewController(t)
	handler := createTestHandler(instance, ctrl, false)
	defer ctrl.Finish()

	req := &pb.UpdateWorkerListRequest{
		WorkerAddresses: workerListStr,
	}
	handledChan := make(chan bool)
	go func() {
		handler.HandleUpdateWorkerListBlockchain(req)
		handledChan <- true
	}()
	select {
	case event := <-eventChan:
		assert.Equal(workerListUpdated, event)
	case <-time.After(5 * time.Millisecond):
		t.Fatalf("No response from UpdateWorkerList in a reasonable amount of time")
	}
	select {
	case handled := <-handledChan:
		assert.True(handled)
	case <-time.After(5 * time.Millisecond):
		t.Fatalf("HandleUpdateWorkerListBlockchain did not finish in a reasonable time")
	}
}

func TestHandleWorkerListUpdatedEvent(t *testing.T) {
	assert := assert.New(t)
	eventChan := make(chan int)
	instance := NewFakeContract(eventChan, assert)
	ctrl := gomock.NewController(t)
	handler := createTestHandler(instance, ctrl, false)
	defer ctrl.Finish()

	event := model.Event{}
	log.Println("Receiving mock WorkerListUpdatedEvent")
	handler.HandleWorkerListUpdatedEvent(&event)

}

func createTestHandler(instance abstract.Contract, ctrl *gomock.Controller, unstable bool) handler.Handler {
	reqID := big.NewInt(1) // currently any reqID will work
	mockArpaMpcClient := mock_arpa_mpc.NewMockArpaMpcClient(ctrl)
	mockReqRepo := mock_request.NewMockRepository(ctrl)
	mockWorkerRepo := mock_worker_list.NewMockRepository(ctrl)
	mockParser := mock_parser.NewMockParser(ctrl)
	if unstable {
		setMockArpaMpcClientBehavior_Unstable(mockArpaMpcClient)
	} else {
		setMockArpaMpcClientBehavior_Valid(mockArpaMpcClient)
	}
	setMockParserBehavior_Valid(mockParser, reqID)
	setMockReqRepositoryBehavior_Valid(mockReqRepo, reqID)
	setMockWorkerRepositoryBehavior_Valid(mockWorkerRepo)
	fakeDispatcher := fake.NewFakeEmptyDispatcher()
	handler := handler.NewHandler(
		mockArpaMpcClient,
		mockParser,
		instance,
		mockReqRepo,
		mockWorkerRepo,
		fakeDispatcher,
	)
	return handler
}

func setMockReqRepositoryBehavior_Valid(r *mock_request.MockRepository, reqID *big.Int) {
	r.
		EXPECT().
		LookUp(gomock.Any()).
		Return(reqID.Int64(), true, nil).
		AnyTimes()

	r.
		EXPECT().
		Insert(gomock.Any(), gomock.Any()).
		Return(nil).
		AnyTimes()
}

func setMockWorkerRepositoryBehavior_Valid(r *mock_worker_list.MockRepository) {

	r.
		EXPECT().
		StoreWorkerList(gomock.Eq(workerList)).
		Return(nil).
		AnyTimes()

	r.
		EXPECT().
		GetWorkerList().
		Return(nil, false, nil).
		AnyTimes()
}

func setMockParserBehavior_Valid(p *mock_parser.MockParser, reqID *big.Int) {

	logRequestReceived := model.LogRequestReceived{
		ReqId:      reqID,
		NumWorkers: big.NewInt(3),
	}

	logComputationStarted := model.LogComputationStarted{
		ReqId: reqID,
	}

	logResultPosted := model.LogResultPosted{
		ReqId: reqID,
	}

	logWorkerListUpdated := model.LogWorkerListUpdated{
		WorkerList: workerList,
	}

	p.
		EXPECT().
		ParseRequestReceivedEvent(gomock.Any()).
		Return(&logRequestReceived, nil).
		AnyTimes()

	p.
		EXPECT().
		ParseComputationStartedEvent(gomock.Any()).
		Return(&logComputationStarted, nil).
		AnyTimes()

	p.
		EXPECT().
		ParseResultPostedEvent(gomock.Any()).
		Return(&logResultPosted, nil).
		AnyTimes()

	p.
		EXPECT().
		ParseWorkerListUpdatedEvent(gomock.Any()).
		Return(&logWorkerListUpdated, nil).
		AnyTimes()
}

func setMockArpaMpcClientBehavior_Valid(c *mock_arpa_mpc.MockArpaMpcClient) {

	ethRes := &pb.RunMpcForBlockchainResponse{
		MpcId:                        "Test MpcId",
		AdditionalWorkerBlockchainAddresses: workerListStr,
	}

	c.
		EXPECT().
		RunMpc(gomock.Any(), gomock.Any(), gomock.Any()).
		Return(nil, nil).
		AnyTimes()

	c.
		EXPECT().
		RunMpcForBlockchain(gomock.Any(), gomock.Any(), gomock.Any()).
		Return(ethRes, nil).
		AnyTimes()
}

func setMockArpaMpcClientBehavior_Unstable(c *mock_arpa_mpc.MockArpaMpcClient) {

	ethRes := &pb.RunMpcForBlockchainResponse{
		MpcId:                        "Test MpcId",
		AdditionalWorkerBlockchainAddresses: workerListStr,
	}

	c.
		EXPECT().
		RunMpc(gomock.Any(), gomock.Any(), gomock.Any()).
		Return(nil, nil).
		AnyTimes()

	c.
		EXPECT().
		RunMpcForBlockchain(gomock.Any(), gomock.Any(), gomock.Any()).
		Return(nil, errors.New("Unstable ArpaMpcClient"))
	c.
		EXPECT().
		RunMpcForBlockchain(gomock.Any(), gomock.Any(), gomock.Any()).
		Return(ethRes, nil)

}

type fakeContract struct {
	eventChan chan int
	assert    *assert.Assertions
}

func NewFakeContract(eventChan chan int, assert *assert.Assertions) abstract.Contract {
	return &fakeContract{
		eventChan: eventChan,
		assert:    assert,
	}
}

// currently assume this is only called once
func (c *fakeContract) UpdateWorkerList(opts *bind.TransactOpts, workersToAdd, WorkersToRemove []common.Address) (*types.Transaction, error) {
	c.assert.Equal(workerList, workersToAdd)
	c.assert.Equal([]common.Address{}, WorkersToRemove)
	c.eventChan <- workerListUpdated // simulate that it is posted onchain
	return &types.Transaction{}, nil
}

func (c *fakeContract) RequestMpc(
	opts *bind.TransactOpts,
	numWorkers *big.Int,
	programHash string,
	_dataProviderWorkers []common.Address,
	delegatedWorker common.Address,
	feeForDataProviderWorker []*big.Int,
	feePerAdditionalWorker *big.Int,
) (*types.Transaction, error) {
	return &types.Transaction{}, nil
}

func (c *fakeContract) StartComputation(opts *bind.TransactOpts, reqID *big.Int, workerList []common.Address) (*types.Transaction, error) {
	time.Sleep(time.Millisecond)
	c.eventChan <- computationStarted // simulate that it is posted onchain
	return &types.Transaction{}, nil
}

func (c *fakeContract) PostResult(opts *bind.TransactOpts, reqID *big.Int, result string) (*types.Transaction, error) {
	time.Sleep(time.Millisecond)
	c.eventChan <- resultPosted // simulate that it is posted onchain
	return &types.Transaction{}, nil
}
