package model

import (
	"crypto/ecdsa"

	"github.com/ethereum/go-ethereum/common"
)

// Account stores a private key and its corresponding address
type Account struct {
	PrivateKey *ecdsa.PrivateKey
	Address    common.Address
}
