package account_test

import (
	"os"
	"testing"

	"gitlab.com/arpachain/mpc/eth/pkg/account"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/stretchr/testify/assert"
)

func TestGetAccount(t *testing.T) {

	const password = "test"

	goPath := os.Getenv("GOPATH")
	testFolderPath := goPath + "/src/gitlab.com/arpachain/mpc/eth/test/"

	tests := []struct {
		file    string
		sk      string
		address common.Address
	}{
		{
			file:    testFolderPath + "data/UTC--2019-01-03T09-50-06.118213000Z--08a9431320f1dfa89ff89381cfe397d301ac9e47",
			sk:      "cbd3a5c1ab2ad5a98f4ec77b7207e5cb6796dc732d0c45ef82e3e1eb73c47e0f",
			address: common.HexToAddress("0x08a9431320F1dFa89fF89381cfe397d301Ac9E47"),
		},
	}
	assert := assert.New(t)
	for _, test := range tests {
		r := account.NewRepository(test.file, password)
		account, err := r.GetAccount()
		assert.NoError(err)
		assert.Equal(test.address, account.Address)
		sk, err := crypto.HexToECDSA(test.sk)
		if err != nil {
			t.Fatalf("Wrong test private key\n%v", err)
		}
		assert.Equal(sk, account.PrivateKey)
	}

}
