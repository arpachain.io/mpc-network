package account

import (
	"errors"
	"io/ioutil"

	"gitlab.com/arpachain/mpc/eth/pkg/model"
	"gitlab.com/arpachain/mpc/eth/pkg/util"
	"github.com/ethereum/go-ethereum/accounts/keystore"
)

// Repository store the calling user's account used for onchain interaction
type Repository interface {
	GetAccount() (model.Account, error)
}

// reference implementation handles a standard encrypted keystore
type repository struct {
	filename string
	auth     string
}

// filename is a key store file, auth is it's password
func NewRepository(filename string, auth string) Repository {
	return &repository{
		filename: filename,
		auth:     auth,
	}
}

func (r *repository) GetAccount() (model.Account, error) {
	keyJSON, err := ioutil.ReadFile(r.filename)
	if err != nil {
		return model.Account{}, err
	}
	key, err := keystore.DecryptKey(keyJSON, r.auth)
	if err != nil {
		return model.Account{}, err
	}
	address, err := util.GetAddrFromSk(key.PrivateKey)
	if err != nil {
		return model.Account{}, err
	}
	if address == key.Address {
		return model.Account{
			PrivateKey: key.PrivateKey,
			Address:    key.Address,
		}, nil
	}
	return model.Account{}, errors.New("private key and address mismatch")
}
