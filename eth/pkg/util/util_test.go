package util_test

import (
	"testing"

	"gitlab.com/arpachain/mpc/eth/pkg/util"
	test_data "gitlab.com/arpachain/mpc/eth/test/data"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/stretchr/testify/assert"
)

func TestGetAddrFromSk(t *testing.T) {

	assert := assert.New(t)

	for i := 0; i < test_data.NumAccounts; i++ {
		sk, err := crypto.HexToECDSA(test_data.PrivateKeys[i][2:])
		if err != nil {
			t.Logf("test failed: %v", err)
			t.FailNow()
			return
		}
		address, err := util.GetAddrFromSk(sk)
		if err != nil {
			t.Logf("test failed: %v", err)
			t.FailNow()
			return
		}
		assert.Equal(test_data.Addresses[i], address.Hex())
	}
}

func TestDifference(t *testing.T) {

	assert := assert.New(t)

	tests := []struct {
		minuends    []common.Address
		subtrahends []common.Address
		difference  []common.Address
	}{
		{
			[]common.Address{
				common.BytesToAddress([]byte("Test 1")),
				common.BytesToAddress([]byte("Test 2")),
				common.BytesToAddress([]byte("Test 3")),
				common.BytesToAddress([]byte("Test 4")),
			},
			[]common.Address{
				common.BytesToAddress([]byte("Test 2")),
				common.BytesToAddress([]byte("Test 4")),
			},
			[]common.Address{
				common.BytesToAddress([]byte("Test 1")),
				common.BytesToAddress([]byte("Test 3")),
			},
		},
		{
			[]common.Address{
				common.BytesToAddress([]byte("Test 1")),
				common.BytesToAddress([]byte("Test 4")),
			},
			[]common.Address{
				common.BytesToAddress([]byte("Test 1")),
				common.BytesToAddress([]byte("Test 2")),
				common.BytesToAddress([]byte("Test 3")),
				common.BytesToAddress([]byte("Test 4")),
			},
			[]common.Address{},
		},
		{
			[]common.Address{
				common.BytesToAddress([]byte("Test 1")),
				common.BytesToAddress([]byte("Test 3")),
				common.BytesToAddress([]byte("Test 4")),
			},
			[]common.Address{
				common.BytesToAddress([]byte("Test 1")),
				common.BytesToAddress([]byte("Test 2")),
				common.BytesToAddress([]byte("Test 4")),
			},
			[]common.Address{
				common.BytesToAddress([]byte("Test 3")),
			},
		},
	}
	for _, test := range tests {
		diff := util.Difference(test.minuends, test.subtrahends)
		assert.Equal(test.difference, diff)
	}
}

func TestSameStringSlice(t *testing.T) {

	assert := assert.New(t)

	tests := []struct {
		a     []string
		b     []string
		equal bool
	}{
		{
			[]string{
				"Test String 1",
				"Test String 2",
				"Test String 3",
			},
			[]string{
				"Test String 3",
				"Test String 1",
				"Test String 2",
			},
			true,
		},
		{
			[]string{
				"Test String 1",
				"Test String 2",
			},
			[]string{
				"Test String 3",
				"Test String 2",
			},
			false,
		},
		{
			[]string{
				"Test String 1",
				"Test String 2",
			},
			[]string{
				"Test String 1",
				"Test String 2",
				"Test String 3",
			},
			false,
		},
		{
			[]string{
				"Test String 1",
				"Test String 2",
				"Test String 3",
			},
			[]string{
				"Test String 1",
			},
			false,
		},
		{
			[]string{
				"Test String 1",
				"Test String 2",
				"Test String 3",
			},
			[]string{},
			false,
		},
		{
			[]string{},
			[]string{
				"Test String 1",
				"Test String 2",
				"Test String 3",
			},
			false,
		},
	}
	for _, test := range tests {
		equal := util.SameStringSlice(test.a, test.b)
		assert.Equal(test.equal, equal)
	}
}
