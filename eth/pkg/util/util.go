package util

import (
	"crypto/ecdsa"
	"errors"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
)

// Get ethereum address from an ecdsa private key
func GetAddrFromSk(sk *ecdsa.PrivateKey) (common.Address, error) {
	pk := sk.Public()
	pkEcdsa, ok := pk.(*ecdsa.PublicKey)
	if ok {
		return crypto.PubkeyToAddress(*pkEcdsa), nil
	}
	return common.HexToAddress("0x0"), errors.New("error casting public key to ECDSA")
}

// an o(n) func returns the set of (minuends - subtrahends)
func Difference(minuends, subtrahends []common.Address) []common.Address {
	mapSubtrahend := map[common.Address]bool{}
	for _, s := range subtrahends {
		mapSubtrahend[s] = true
	}
	diff := []common.Address{}
	for _, m := range minuends {
		if _, ok := mapSubtrahend[m]; !ok {
			diff = append(diff, m)
		}
	}
	return diff
}

// an o(n) func to check if two string slice contain the same elements
// if a or b contains a same element more than once, the return value
// may be arbitrary
func SameStringSlice(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	diff := make(map[string]bool, len(a))
	for _, str := range a {
		diff[str] = true
	}
	for _, str := range b {
		// If the str is not in diff bail out early
		if _, ok := diff[str]; !ok {
			return false
		}
		delete(diff, str)
	}
	if len(diff) == 0 {
		return true
	}
	return false
}
