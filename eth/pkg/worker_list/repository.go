package worker_list

import (
	"encoding/json"

	db_utils "gitlab.com/arpachain/mpc/persistent/pkg/db"
	"github.com/dgraph-io/badger"
	"github.com/ethereum/go-ethereum/common"
)

const (
	Key       = "worker_list" // There is only one key-value pair, yet Key still has to be used with
	KeyPrefix = "ethworker_"
)

type Repository interface {
	StoreWorkerList(workerList []common.Address) error
	GetWorkerList() ([]common.Address, bool, error)
}

// a BadgerDB-based implementation
type badgerRepository struct {
	db *badger.DB
}

// NewBadgerRepository returns a BadgerDB-based implementation
func NewBadgerRepository(db *badger.DB) Repository {
	return &badgerRepository{
		db: db,
	}
}

func (r *badgerRepository) StoreWorkerList(workerList []common.Address) error {
	workerListBytes, err := json.Marshal(workerList)
	if err != nil {
		return err
	}
	err = db_utils.CreateObject(r.db, KeyPrefix, Key, workerListBytes)
	if err != nil {
		return err
	}
	return nil
}

// Look up the worker list
// If the worker list is not found in the db, the second argument returned will be false, without an error.
// Therefore, first check the error, then check if the value is found, finally use the value.
func (r *badgerRepository) GetWorkerList() ([]common.Address, bool, error) {
	workerList := make([]common.Address, 0)
	exists, err := db_utils.GetObject(r.db, KeyPrefix, Key, &workerList)
	if err != nil {
		return nil, false, err
	}
	if !exists {
		return nil, false, nil
	}
	return workerList, true, nil
}
