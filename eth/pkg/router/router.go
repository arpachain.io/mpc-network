package router

import (
	"log"

	"gitlab.com/arpachain/mpc/eth/pkg/handler"
	"gitlab.com/arpachain/mpc/eth/pkg/model"
	"gitlab.com/arpachain/mpc/eth/pkg/transaction"
)

// Router routes events to its corresponding handler based on event type
type Router interface {
	Route(event *model.Event)
}

// Reference implementation:
type router struct {
	handler       handler.Handler
	logSigHashMap model.LogSigHashMap
	repository    transaction.Repository
}

// NewRouter returns an instance of eth.Router
func NewRouter(handler handler.Handler, logSigHashMap model.LogSigHashMap, repo transaction.Repository) Router {
	return &router{
		handler:       handler,
		logSigHashMap: logSigHashMap,
		repository:    repo,
	}
}

func (r *router) Route(event *model.Event) {
	isDupEvent, err := r.repository.HasKey(event.Log.TxHash)
	if err != nil {
		log.Printf("Eth router: error while trying to lookup an event tx: %v\n", err)
		return
	}
	// event has already be handled
	if isDupEvent == true {
		return
	}

	err = r.repository.Insert(event.Log.TxHash)
	if err != nil {
		log.Printf("Eth router: error while trying to store an event tx: %v\n", err)
		return
	}
	switch event.Log.Topics[0].Hex() {
	case r.logSigHashMap.GetRequestReceivedSigHash().Hex():
		r.handler.HandleRequestReceivedEvent(event)
	case r.logSigHashMap.GetComputationStartedSigHash().Hex():
		r.handler.HandleComputationStartedEvent(event)
	case r.logSigHashMap.GetResultPostedSigHash().Hex():
		r.handler.HandleResultPostedEvent(event)
	case r.logSigHashMap.GetWorkerListUpdatedSigHash().Hex():
		r.handler.HandleWorkerListUpdatedEvent(event)
	}
}
