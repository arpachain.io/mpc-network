API_DIR=api
PACKAGE_PATH=gitlab.com/arpachain/mpc/eth
ETH_PATH=$(GOPATH)/src/$(PACKAGE_PATH)

GO_SRC = $(shell find . -name '*.go') \
         test/e2e/ethereum_integration_e2e.go \
		 api/arpa_mpc.pb.go \
		 sol/ArpaMpc.sol.go

TEST_ENV = test/node_modules/ganache-cli

all:

api/arpa_mpc.pb.go: ../proto/arpa_mpc.proto
	mkdir -p $(API_DIR)
	protoc -I ../proto/ ../proto/arpa_mpc.proto --go_out=plugins=grpc:$(API_DIR)

sol/ArpaMpc.sol.go: ../smart_contract/contracts/ArpaMpc.sol
	./scripts/generate-abi-from-sol.sh

$(TEST_ENV): test/package.json
	cd test/ ; npm install

mock: mock/request/mock_repository.go \
      mock/worker_list/mock_repository.go \
	  mock/transaction/mock_tx_opts.go \
	  mock/parser/mock_parser.go\
	  mock/abstract/mock_eth_client.go \
	  mock/api/mock_arpa_mpc_client.go

mock/request/mock_repository.go: pkg/request/repository.go
	mockgen \
	-source=$(ETH_PATH)/pkg/request/repository.go \
	-destination=$(ETH_PATH)/mock/request/mock_repository.go

mock/worker_list/mock_repository.go: pkg/worker_list/repository.go
	mockgen \
	-source=$(ETH_PATH)/pkg/worker_list/repository.go \
	-destination=$(ETH_PATH)/mock/worker_list/mock_repository.go

mock/transaction/mock_tx_opts.go: pkg/transaction/tx_opts.go
	mockgen \
	-source=$(ETH_PATH)/pkg/transaction/tx_opts.go \
	-destination=$(ETH_PATH)/mock/transaction/mock_tx_opts.go

mock/parser/mock_parser.go: pkg/parser/parser.go
	mockgen \
	-source=$(ETH_PATH)/pkg/parser/parser.go \
	-destination=$(ETH_PATH)/mock/parser/mock_parser.go

mock/abstract/mock_eth_client.go: pkg/abstract/eth_client.go
	mockgen \
	-source=$(ETH_PATH)/pkg/abstract/eth_client.go \
	-destination=$(ETH_PATH)/mock/abstract/mock_eth_client.go

mock/api/mock_arpa_mpc_client.go: api/arpa_mpc.pb.go
	mockgen \
	-destination=$(ETH_PATH)/mock/api/mock_arpa_mpc_client.go \
	-package=mock_arpa_mpc \
	$(PACKAGE_PATH)/api ArpaMpcClient

clean:
	-rm api/arpa_mpc.pb.go
	-rm sol/*
	-rm -rf mock/*
	-rm -rf test/node_modules

check: $(GO_SRC) mock $(TEST_ENV) 
	# go vet ./...
	go test ./... -cover
	cd test/e2e ; go run ethereum_integration_e2e.go


